/*!
 * gulp instructions
 *	(1) Install dependencies
 * 		$ npm install
 *	(2) Change environment variables from "custom settings" section, to match your configuration (ie. URL, theme name);
 */
process.env.DISABLE_NOTIFIER = true;
// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    spritesmith = require('gulp.spritesmith'),
    sourcemaps = require('gulp-sourcemaps'),
    bs = require('browser-sync'),
    del = require('del');

// Environmental Variables
/** Custom Settings 		**/
var theme = 'theme',
	url = 'http://www.awesomeswords.local/';
/** End Custom Settings 	**/

var	src = 'views/'+theme+'/src',
    files = ['views/'+theme+'/pub/**/*','*.php'],
    dist = 'views/'+theme+'/pub';

// Styles
gulp.task('styles', function() {
  return sass(src+'/css/main.scss', {sourcemap: true, style: 'compact'})
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(gulp.dest(dist+'/css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(cssnano({zindex:false}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist+'/css'))
    .pipe(notify({ message: 'Styles task complete' }));
});

// Sprites
gulp.task('sprite', function () {
    var spriteData = gulp.src(src+'/sprites/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: '../img/sprite.png',
            cssName: '_sprite.scss',
            padding: 20
        }));
    spriteData.img.pipe(gulp.dest(src+'/img'));
    spriteData.css.pipe(gulp.dest(src+'/css'));
});

// Scripts
gulp.task('scripts', function() {
  return gulp.src(src+'/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest(dist+'/js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(dist+'/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Images
gulp.task('images', function() {
  return gulp.src(src+'/img/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(dist+'/img'))
    .pipe(notify({ message: 'Images task complete' }));
});

// Clean
gulp.task('clean', function() {
  return del([dist+'/css', dist+'/js', dist+'/img']);
});

// Default task
gulp.task('default', function() {
  gulp.start('sprite','styles', 'scripts', 'images');
});

// Browser sync
gulp.task('browser-sync', ['styles'], function() {
  bs.init({
    proxy: url
  });
});

// Watch
gulp.task('watch', ['browser-sync'], function() {

  // Watch .scss files
  gulp.watch(src+'/css/**/*.scss', ['styles']);

  // Watch .js files
  gulp.watch(src+'/js/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch(src+'/img/**/*', ['images']);

  // Watch sprites
  gulp.watch(src+'/sprites/**/*.png', ['sprite']);

  // Watch any files in dist/, reload on change
  gulp.watch(files).on('change', bs.reload);

});
