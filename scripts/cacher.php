<?php
	
	try{
		// ~1.5 hrs to run
		$ebay = new \Modules\Ebay\Controllers\EbayController;
		$eb_cache = $ebay->setCache('Categories','All');
		//$eb_cache = $ebay->setCache('FindByCat','All');
		($eb_cache===TRUE)? print('Ebay success') : print('Ebay failure');
	} catch(\Exception $e) {
		print('Caught exception: Ebay Category Cache '.$e->getMessage()."\n");
	}
	
	try{	
		// ~2.5 hours to run
		$amazon = new \Modules\Amazon\Controllers\AmazonController;
		$am_cache = $amazon->setCache('Categories','All');
		//$am_cache = $amazon->setCache('FindByCat','All');
		($am_cache===TRUE)? print('Amazon success') : print('Amazon failure');
	} catch(\Exception $e) {
		print('Caught exception: Amazon Category Cache '.$e->getMessage()."\n");
	}
	
	exit;

?>