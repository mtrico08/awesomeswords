<?php
	$return = ['status'=>false];
	// REFACTOR 0:  Is there a way of injecting or preserving the previous rendered object? Instantiating the object each time starts us back at position 0 when using pagination, so we have to manually inject the page.
	$this->obj = (!isset($this->obj))? $this->getModule('product') : $this->obj;
	$page = (!isset($this->params->page))? '0' : $this->params->page;
	$page++;
	$items = $this->obj->formatItemsByCategory($this->params->var,$this->params->num,$page);
	if(!empty($items)){
		$return = [
			'status'=>true,
			'page'=>$page,
			'view'=>$items,
		];
	} else {
		$return['message'] = 'No more products';
	}
	echo json_encode($return);
?>
