<?php
namespace Modules\Yahoo\Controllers;

class YahooController extends \Modules\Yahoo\Models\Yahoo {

	private $call_signs;
	public $num;
	public $quote;
	public $news;
	protected $arg;
	public $metric;
	public $type;
	
	public function __construct() {
		$this->call_signs = func_get_args();
		$i = 0;
		$signs = '';
		$this->num = count($this->call_signs);
		foreach($this->call_signs as $sign){
			if($i==$this->num-1){
				$signs .= $sign;
			} else {
				$signs .= $sign.',';
			}
			$i++;
		}
		$this->call_signs = $signs;
		return parent::getQuote();
	}
	
	public function getAll(){
		//$this->call_signs = //all signs
		//REFACTOR:  Grab the indexes from the model, as symbols. Database should have index normalization table
		foreach (array('^IXIC','^GSPC','^DJI') as $index){
			self::getQuote($index);
		};
		return getQuote($this->call_signs);
	}
	
	public function getQuote() {
		//Google URL: http://www.google.com/finance/info?infotype=infoquoteall&q=SYMBOL
		$api_url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20("'.$this->call_signs.'")&format=json&env=http://datatables.org/alltables.env';
		// Caching existing
		//$this->cache = './cache/'.sha1($this->api_url).'_'.$this->call_signs.'_'.time().'.json';
		$cache = './cache/quote_'.$this->call_signs.'_'.time().'.json';
		if(file_exists($cache) && filemtime($cache) > time() - 60*5){
			// If a cache file exists, and it is newer than 5 minutes, use it
			$this->quote = json_decode(file_get_contents($cache));
		} else {
			$this->quote = json_decode((file_get_contents($api_url)));
			file_put_contents($cache,json_encode($this->quote));
		}
		return $this->quote;
	}
	
	public function getNews($signs) {
		$api_url = 'http://feeds.finance.yahoo.com/rss/2.0/headline?s='.$this->call_signs.'&region=US&lang=en-US';
		// Caching existing
		$cache = './cache/news_'.$this->call_signs.'_'.time().'.xml';
		if(file_exists($cache) && filemtime($cache) > time() - 60*5){
			// If a cache file exists, and it is newer than 5 minutes, use it
			$this->news = json_decode(file_get_contents($cache));
		} else {
			$this->news = json_decode((file_get_contents($api_url)));
			$xml = simplexml_load_string(file_get_contents($api_url), "SimpleXMLElement", LIBXML_NOCDATA);
			$json = json_encode($xml);
			file_put_contents($cache,json_encode($this->news));
		}
		return $this->news;
	}
	
	//---------------------START REMOVE----------------------//
	
	/*public function cleaner($arg){
		$result = '';
		$arg = str_replace(' ','-', $arg);
		// replaces spaces with hyphens
		$arg = preg_replace('/[^A-Za-z0-9\-\!]/', '', $arg);
		//$arg = preg_split('/(?=[A-Z])/',$arg);
		$arg = preg_split('/[-]/',$arg);
		// now splits words by the hyphen to shorten them
		foreach ($arg as $str) {
			$str = strtolower($str);
			$str = ucfirst($str);
			$result .= $str;
		};
		$arg = $result;
		//$arg = str_replace('-','', $result);
		return $arg;
	}*/
	
	//---------------------END REMOVE------------------------//

	
	public function listing(){
		$id = 0;
		$result = '';
		$allfilters = '';
		$filterarray = self::filterarray();
		$matches = array();
		foreach ($filterarray as $hashtag) {
			$allfilters .= $hashtag.' ';
		}
		$result .= "\t".'';
		foreach ($this->jsonData->data as $key=>$value) {
			if(isset($value->caption)){
				$description = $value->caption->text;
			}
			if(isset($value->tags)&&!empty($value->tags)){
				$matches = $value->tags;
				$tags = '';
				foreach ($matches as $match) {
					$match = self::cleaner($match);
					$tags .= $match.' ';
				};
				$timestamp = $value->caption->created_time;
			} else {
				$tags = '';
				$timestamp = time();
			}
			$result .= "\t".'';
			$id++;
		}
		echo $result;
	}
	
	//---------------------START REMOVE----------------------//
	
	/*public function sortArray(&$items, $key, $descending = false){
	  if (is_array($items)){
		return usort($items, function($a, $b) use ($key, $descending){
		  $cmp = $a['type']-$b['type'];
		  return $descending? -$cmp : $cmp;
		});
	  }
	  return false;
	}*/

	//---------------------END REMOVE------------------------//
	
	public function filtermultiarray($type='date',$order=true){
			$desc = ($order=='descending')? true : false;
			if($type=='date'){
				$itm=false;
				$metric='created_time';
			}else{
				$itm='likes';
				$metric='count';
			}
			$result = array();
			$data = $this->jsonData->data;
			usort($data, function($a, $b) use ($itm, $metric, $desc) {
				$desc = ($desc) ? -1 : 1;
				if($itm!==false){
					return ($a->$itm->$metric > $b->$itm->$metric) ? $desc : ($desc*-1);
				} else {
					return ($a->$metric > $b->$metric) ? $desc : ($desc*-1);
				}
			});
			foreach ($data as $key=>$value) {
				if(isset($value->tags)){
					$result[] = $value->tags;
				} else {
					$result[] = '... Whoops, sorry. Nothing available';
				}
			}
			return $result;
	}
	
	public function filterarray($type='date',$number='100',$order='descending') {
		$result = array();
		$allfilters = self::filtermultiarray($type,$order);
		$i = 0;
		foreach ($allfilters as $key=>$value) {
			foreach ($value as $key=>$match) {
				//$matches = self::splithash($match);
				$i++;
				if($i>=intval($number)){
					break;
				} else {
					$result[] = $match;
				}
			}
		}
		return array_unique($result);
	}
	
	//---------------------START REMOVE----------------------//
	
	/*public function filt($arg){
		$result = self::filtermultiarray()[$arg];
		return $result;
	}
	
	public function endproduct($arg) {
		$result = '';
		$data = self::filt($arg);
		foreach ($data as $item) {
			$result .= $item.' ';
		}
		return $result;
	}*/
	
	//---------------------END REMOVE------------------------//
}

?>