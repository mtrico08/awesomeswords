<?php
namespace Modules\Ebay\Controllers;

use \Modules\Ebay\Models\Ebay as Ebay;

class EbayController{
	
	public $ebay;
	public $array_results;
	public $helper;

	public function __construct(){
		$this->ebay = new Ebay;
		$this->array_results = array();
		$this->helper = $this->ebay->helper;
	}

	/** setCache triggers cache build **/

	public function setCache($type,$var){
		return $this->helper->setCache($type,$var,'Ebay');
	}

	/** getItemsFromRef performs a lookup for a value ($var) on all items matching a
	/*	specified index's value, and organization type, such as "categories"
	/*	Accepts "extended" param: "basic" is fastest, "extended" includes more details
	/*	Accepts "position" to set the maximum number of results
	**/

	public function getItemsFromRef($index='name',$type='categories',$var=NULL,$count=100,$position=1,$extended='basic'){
		if(in_array($type,$this->ebay->operations)){
			$operation = $type;
		} elseif ($index=='id') {
			if(is_numeric($var)){
				$operation = $type = 'find';
			} else {
				throw new \Exception('ID must be numeric');
			}
		} else {
			$operation = 'findByCat';
		}
		$results = $this->ebay->getRef($index,$type,$var,$position,$count);
		
		if($type=='categories'){
			// REFACTOR:  Do we need any of this anymore 44-67?
			$results = array();
			$basic_type = array(); // IE. basic "categories"
			// REFACTOR: pass exclude categories results to keywords?
			$basic_keywords = $this->ebay->getItems($var,$operation,$position);
			
			// We ordered our array by weight, so we just need to drop that by diving 2 levels deep
			foreach($array as $ordered_block){
				foreach($ordered_block as $parent){
					foreach($parent as $child){
						$basic_type = array_merge($basic_type,(array)$this->ebay->getItems($child,$operation,$position));
					}
				}
			}

			if($extended!='basic'){
				foreach(array_unique(array_merge($basic_keywords,$basic_type),SORT_REGULAR) as $item){
					$results[] = $this->ebay->getItem($item->itemId);
				}	
			} else {
				$results = array_unique(array_merge($basic_keywords,$basic_type),SORT_REGULAR);
			}
		}
		return self::formatItems($results->$var);
	}
	
	/** resetOffset can format arrays of objects for properly merging.
	**/
	public function resetOffsetObject($objarray){
		if(empty($objarray)){
			return array();
		} elseif(isset($objarray->ack)){
			return array($objarray->item);
		} else {
			return $objarray;
		}
	}

	/** formatItems builds an array input to make all values compatible with a standard product object 
	**/
	// REFACTOR - see country and zip... get can get from browser gps
	// REFACTOR - Drop products where ExcludeShipToLocation is current location
	public function formatItems($var, $ctry = 'US', $zip = '31405'){
		foreach((array)$var as $item) {
			// If we're not at the right level, we'll recursively foreach until we get there.
			if(!isset($item->itemId)&&!isset($item->ItemID)){
				self::formatItems($item);
			} else {
				$id = $this->helper->caseInsensitivePropertyAccess($item,'itemId');
				// Ignore items that have no image, are near expiration, or already exist in array
				$endtime = (isset($item->listingInfo))? strtotime($item->listingInfo->endTime) : strtotime($item->EndTime);
				if (	null === ($img = $this->helper->caseInsensitivePropertyAccess($item,'GalleryURL'))
						||((isset($endtime) && $endtime !== FALSE) && ($endtime <time()))
						||array_key_exists($id,$this->array_results)
					) {
					continue;
				} else {
					try{
						$url = (isset($item->ViewItemURLForNaturalSearch)) ? $item->ViewItemURLForNaturalSearch : $item->viewItemURL;
					} catch (Exception $e) {
						continue;  // If no URL, there's no point to the listing, so we just skip it
					}
					
					if(FALSE == $title = $this->helper->caseInsensitivePropertyAccess($item,'Title')){
						$title = '';
					}
					
					// Replace the IMG with the higher res photo, if there is one
					$img = (isset($item->PictureURL))? $item->PictureURL : $img;
					
					if($categoryName = isset($item->PrimaryCategoryName)){
						//$categories = explode(':',$categoryName);
						$categories = str_replace(':',',',$categoryName);
					} else {
						$categories = (isset($item->primaryCategory->categoryName))? $item->primaryCategory->categoryName : '';
					} 

					// Start - Calculate the price from the base and shipping price.
					if(isset($item->shippingInfo->shippingServiceCost)){
						$ship = sprintf("%01.2f", $item->shippingInfo->shippingServiceCost);
					} else {
						$shipping = $this->ebay->getShipping($id,array('DestinationCountryCode'=>$ctry,'DestinationPostalCode'=>$zip));
						$ship = sprintf("%01.2f", $shipping->ShippingCostSummary->ShippingServiceCost);
					}
					$base = (isset($item->sellingStatus))? $item->sellingStatus->convertedCurrentPrice : $item->ConvertedCurrentPrice;
					$base = sprintf("%01.2f", $base);
					$price = sprintf("%01.2f", ((float)$base + (float)$ship));
					// Account for rare case where price and shipping currencies differ
					if($priceCurr = isset($item->sellingStatus->convertedCurrentPrice->currencyId)&& $shipCurr = isset($item->shippingInfo->shippingServiceCost->currencyId)){
						$currency = ((string)$priceCurr == (string)$shipCurr) ? (string)$priceCurr : "(string)$priceCurr / (string)$shipCurr";
					} else { $priceCurr = '$'; }
					// End - Calculate price
					
					$this->array_results[$id] = (object) array ('name'=>$title,
																'url'=>$url,
																'img'=>$img,
																'currency'=>$priceCurr,
																'price'=>$price,
																'cat'=>$categories,
																'source'=>'ebay');
				}
			}
		}
		return $this->array_results;
	}

	/** Expanded:
	[ItemID][EndTime][ViewItemURLForNaturalSearch][ListingType][Location][GalleryURL][PictureURL][PrimaryCategoryID][PrimaryCategoryName][BidCount][ConvertedCurrentPrice][ListingStatus][TimeLeft][Title][Country][AutoPay][ConditionID][ConditionDisplayName][QuantityAvailableHint][QuantityThreshold]
	**/
	/** Basic: 
	[itemId][title][globalId]
	[primaryCategory]=>Object([categoryId][categoryName])
	[galleryURL][viewItemURL][paymentMethod][autoPay][postalCode][location][country]
	[shippingInfo]=>Object([shippingServiceCost][shippingType][shipToLocations][expeditedShipping][oneDayShippingAvailable][handlingTime])
	[sellingStatus]=>Object([currentPrice][convertedCurrentPrice][sellingState][timeLeft])
	[listingInfo]=>Object([bestOfferEnabled][buyItNowAvailable][startTime][endTime][listingType][gift])
	[returnsAccepted]
	[condition]=>Object([conditionId][conditionDisplayName])[isMultiVariationListing][topRatedListing])
	**/

}
?>