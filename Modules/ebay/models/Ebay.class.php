<?php
namespace Modules\Ebay\Models;

use Modules\Product\Models\Product as Product;

class Ebay extends Product {
	/** http://developer.ebay.com/DevZone/shopping/docs/Concepts/ShoppingAPI_FormatOverview.html#AffiliateURLParameters 
	**/
	private $shopping_endpoint;
	private $shopping_version;
	private $category_version;
	private $finding_endpoint;
	private $finding_version;
	private $responseEncoding;	
	private $appID;
	private $netId;
	private $trackId;
	private $custId;
	private $globalID;
	private $sitearray;
	private $siteID;
	private $timestamp;
	private $success;
	private $array_results;
	private $id;
	private $max_entries;
	private $position;
	private $end_position;
	private $per_page;
	/** Must remain public **/
	public $helper;
	public $operations;
	
	public function __construct(){
		$this->helper = parent::getHelper();
		$this->siteID = $this->sitearray[$this->globalID];
		return self::setVars();
	}

	/** setVars is critical, it sets all variables used in all model functions
	**/
	public function setVars(){
		$this->shopping_endpoint = 'http://open.api.ebay.com/shopping';  // Shopping
		$this->shopping_version = '667';   // Shopping API version number
		$this->category_version = '667';
		
		$this->finding_endpoint = 'http://svcs.ebay.com/services/search/FindingService/v1';  // Finding
		$this->finding_version = '1.4.0';   // Finding API version number
		
		$this->responseEncoding = 'XML';   // Format of the response
		
		/** Usable on all eBay related classes and functions 																**/
		/** http://developer.ebay.com/DevZone/shopping/docs/Concepts/ShoppingAPI_FormatOverview.html#AffiliateURLParameters **/
		
		$this->appID   = 'ArcaneSt-AwesomeS-PRD-e99eeebc0-6908031c';
		$this->netId = '9'; // eBay Partner Network is value 9
		$this->trackId = '5337921693';
		$this->custId = 'awesomeswords';

		$this->globalID = 'EBAY-US';
		$this->sitearray = array(
			'EBAY-US' => '0',
			'EBAY-ENCA' => '2',
			'EBAY-GB' => '3',
			'EBAY-AU' => '15',
			'EBAY-DE' => '77',);
			
		$this->array_results = array();
		$this->id = -1;
		$this->i = 0;
		$this->operations = array('categories','find','ship','findByCat');
		$this->end_position = $this->per_page = $this->max_entries = 100;
		$this->position  = 1;
		
		return true;
	}

	/** 
	/*	setClient 
	/*	Builds the API curl
	**/
	protected function setClient($operation, $cat = -1, $position = NULL, $var = NULL, $sort = 'BestMatch'){
		/** Accepts operation categories, ship, findByCat, and find **/
		/** sort accepts Price, Geo distance, Start Time, and End Time, as well as Best Match and more **/
		if(!in_array($operation,$this->operations)){
			throw new \Exception('Incorrect API Search Type.');
		}
		
		$tracking = "affliate.networkId=$this->netId&"
				. "affliate.trackingId=$this->trackId&"
				. "affliate.customId=$this->custId";
		
		if($operation == 'categories'|| $operation == 'find' || $operation == 'ship'){	
			/** EXAMPLE CALL CATEGORIES						**/
			/** setClient('categories', '1', NULL, ChildCategories)	**/
			if(($var!=null)&&(!is_array($var))){
				$var = ($operation == 'categories')? "IncludeSelector=$var&" : '';
			} elseif(is_array($var)) {
					$option_string = '';
				foreach($var as $key=>$argument){
					$option_string .= $key.'='.$argument.'&';
				}
				if($operation == 'ship'){
					$option_string .= 'IncludeDetails=true&';
				}
				$var = $option_string;
			} else {
				$var = '';
			}
			
			if($operation == 'categories'){
				$conditional = "CategoryID=$cat&";
				$callname = "callname=GetCategoryInfo&";
			} else if ($operation == 'find'){
				$conditional = "ItemID=$cat&";
				$callname = "callname=GetSingleItem&";
			} else if ($operation == 'ship'){	
			// REFACTOR: If one more lookup type uses ItemID, break that into its own OR condition
				$conditional = "ItemID=$cat&";
				$callname = "callname=GetShippingCosts&";	
			}
			
			$results = "$this->shopping_endpoint?"
				. $callname
				. "responseencoding=$this->responseEncoding&"
				. "appid=$this->appID&"
				. "siteid=$this->siteID&"
				. "version=$this->category_version&"
				. $conditional
				. $var
				. $tracking;
				
		} else if ($operation == 'findByCat') {
			/** 			EXAMPLE OPTIONS (VAR) & CAT CALL
			/**		array(	0=>array(Condition=>array(New,Old,Used)),
			/**				1=>array(MaxPrice=>array(50.0),Currency=>array(USD,EURO)))
			/**		array( 	'keyword' => 'text',
			/**				'category' => array('0001','0002','0003'))
			/** 			END EXAMPLE CALL			**/
			$position = (isset($position))? $position : $this->position;
			$position = "paginationInput.pageNumber=$position&paginationInput.entriesPerPage=$this->max_entries&";
			$sort = ($sort)? "sortOrder=$sort&":'';
			if($var){
				if(!is_array($var)){
					throw new \Exception('Options Must Be An Array.');
				} else {
					$opt = '';
					$f = 0; $v = 0;
					// Filters are hugely useful for detailed searches but categories and keyword cannot be filters
					// ExcludeCategory may permit to exclude repeated results from categories search (ie. in keywords)
					foreach($var as $filter){	// Query has many filters.  Filters have one name and may have one param.  Name and param have many values.
						foreach($filter as $key=>$values){
							$first = key(reset($array));
							$type = ($key == $first)? 'name':'paramName';
							$opt .= "&itemFilter(".$f.").".$type."=".$key;
							foreach($values as $value){				
									$term = ($type=='name')? 'value':'paramValue';
									$opt .= "&itemFilter(".$f.").".$term."(".$v.")=".$value;
								$v++;
							}
						}
						$f++;
					}
				}
			} else {
				$opt = '';
			}
			if (is_array($cat)){
				$condition = "";
				if(isset($cat['keyword'])){
					$keyword = $cat['keyword'];
					$condition = "keywords=$keyword&";
				}
				if(isset($cat['category'])){
					$category = $cat['category'];
					if (is_array($category)){
						foreach($category as $catID){
							$condition .= "categoryId=$catID&";
						}
					} else {
						$condition .= "categoryId=$category&";
					}
				}
			} else {
				$condition = (is_numeric($cat))? "categoryId=$cat&" : "keywords=$cat&";
			}
			$op = 'findItemsAdvanced';
			$results = "$this->finding_endpoint?"
				. "OPERATION-NAME=$op&"
				. "version=$this->finding_version&"
				. "GLOBAL-ID=$this->globalID&"
				. "SECURITY-APPNAME=$this->appID&"
				. "RESPONSE-DATA-FORMAT=$this->responseEncoding&"
				. $opt
				. $position
				. $sort
				. $condition
				. $tracking;
				
		}
		return $results;
	}

	/** getClient validates and retrieves client curl address. Returns as XMLElement Objects
	/*	"Var" is a searchable index
	**/
	public function getClient($apicall,$var=NULL){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apicall);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$xml = curl_exec($ch);
		if(curl_exec($ch) === false)
			{ echo 'Curl error: ' . curl_error($ch); }
		$resp = simplexml_load_string($xml);
		//$resp = simplexml_load_file($apicall);
		$x = 0;
		$results = '';
		$valid = self::verifyRequest($resp);
		foreach($resp as $block){
			if((isset($var))&&(key($block)===$var)){
				$results = $block;
				$x++;
			}
		}
		// If there exists one object of Index name, use that as results, otherwise use the standard response.
		if(!$x>0){
			$results = $resp;
		}
		if(($valid)&&(isset($this->timestamp))){
			return $results;
		} else {
			print($apicall); die();
			throw new \Exception('Failure Ebay');
		}
	}
	
	/** verifyRequest function sets a "success" and "timestamp" value and returns as true or false.
	**/
	public function verifyRequest($resp){
		$i = 0;
		if(isset($resp)){
			if(isset($resp->ack)){
				if($resp->ack=='Success'){
					$this->success = TRUE;
				}
			}
			foreach($resp as $block){
				if($i==0){ $this->timestamp = reset($block); }
				if($this->success == TRUE){
					break;
				} else if(($i==1)&&(reset($block)=='Success')){
					$this->success = TRUE;
					break;
				}
				$i++;
			}
		}
		if($this->success == TRUE){
			return TRUE;
		} else {
			$this->success = FALSE;
			return FALSE;
		}
	}
	
	/** getCategories returns specified Category and default includes its children.  -1 is top level.
	**/
	public function getCategories($id=-1,$child=1) {
		$child=($child==1)?'ChildCategories':'';
		$apicall = self::setClient('categories',$id,NULL,$child);
		$resp = self::getClient($apicall,'Category');	// NOTE: "Category" is the correct Key, not Categories
		return $resp;
	}

	/** setCategories returns all categories entirely 							**/
	/** Children should remain <=1 as the parent itself counts as its own child	**/
	// REFACTOR - What's a leaf category?
	public function setCategories($var=NULL) {
		$this->id = (isset($var)) ? $var : $this->id;
		$children = self::getCategories($this->id,1);
		foreach($children as $child){
			$this->id = (string)$child->CategoryID;
			if (false !== $key = array_search($this->id, array_column($this->array_results, 'id'))){ continue; } else {
				$name = $child->CategoryName;
				$parent = $child->CategoryParentID;
				$this->array_results[] = array('id'=>$this->id,'name'=>(string)$name,'parent'=>(string)$parent,'source'=>'ebay');
			}
			if(count($children)<=1){
				continue;
			} else {
				self::setCategories($this->id);
			}
		}
		return $this->array_results;
	}
	
	/** 
	 *	setPagination 
	 *	Sets the number of results per page and the page that we're working on. Pagination is different for eBay and Amazon, so this method is unique to each.
	**/
	public function setPagination($pos,$count){
		$this->position = $pos;
		$this->max_entries = $count;
		$this->end_position = $count;
		return true;
	}
	
	/** setFindByCat
	 *	returns all items matching filters, by category ID (numeric).
	 *	May accept up to 3 category ID's and 1 keyword.
	 *	Var is keyword, Cat is categories.
	**/
	public function setFindByCat($var='all',$cat=NULL){
		if(isset($cat)&&(!is_array($cat)||!empty($cat))){
			$results[$var] = self::getItems($cat,'findByCat',NULL,$var);
		} else {
			$results[$var] = self::getItems(NULL,'findByCat',NULL,$var);
		}
		return $results;
	}

	/** getRef gets ID's of type by index.  
	/*	Used with "Name" to be paired with getCategories or getItems for further details.
	/*	Accepts $type for search and data type, such as "categories".  Accepts $var for ID lookup.
	/*	Accepts $index for reference.
	**/
	// REFACTOR: getRef move to ProductModel?  Need to add a param for source, to pass to all those others
	// if(strtolower($var)=='all'){$var ='-1';} is Ebay specific
	public function getRef($index='name',$type='categories',$var='all',$position=1,$count=100){
		$array_results = array();
		if(strtolower($type)=='find'){
			// ItemLookup (by ID) REFACTOR 0
			$array_results = $this->helper->getData($type,$var,'Ebay',NULL,$position,$count,TRUE);
		} else if(strtolower($type)=='findbycat'){
			// ItemSearch: Filtered lookup by cat ID.
			// If there are any relevant category matches, we pass them as params in weighted order.
			$matching_cats = self::getRef('name','categories',$var);
			$categories = array();
			if(isset($matching_cats)&&!empty($matching_cats)){
				// No need for a foreach loop here, we can just pass the top 3 results.
				$matching_cats = array_slice($matching_cats, 0, 3);
				foreach($matching_cats as $weighted){
					foreach($weighted as $value){
						$categories = array_merge($categories,$value);
					}
				}
				$categories = array_slice($categories, 0, 3);
				$array_results = $this->helper->getData($type,$var,'Ebay',$categories,$position,$count,TRUE);
			} else {
				// If there are no matching categories, we'll perform the default lookup across all categories.
				$array_results = $this->helper->getData($type,$var,'Ebay',NULL,$position,$count,TRUE);
			}
		} else if(strtolower($type)=='categories'){
			$all = $this->helper->getData($type,'-1','Ebay',NULL,NULL,NULL,TRUE);
			// Returns a 1 level array of all types (categories) matching value. Includes ID and Index
			foreach ($all as $item) {
				if($rank = $this->helper->getCompare($var,$item->$index)){
					$array_results[$rank][$item->parent][] = $item->id;
					// Sort by weight
					krsort($array_results);
				}
			}
		} else if(strtolower($type)=='similar'){
			// SimilarityLokup (by ID) REFACTOR
		}
		return $array_results;
	}

	/** getItems
	 *	Returns all items by numerical ID or Keyword.
	 *	Data is limited per item, use getItem for more detail
	 *  Here, Var amd Cat (keywords and categories) are combined
	 *  to a single param, so the 4th apicall param can remain an "options" call
	 *	for additional filters. REFACTOR: options isn't built in, yet.
	**/
	public function getItems($cat,$operation='findByCat',$position=NULL,$keyword=NULL) {
		$catparam = (isset($cat))? array('category'=>$cat) : array();
		$keyparam = (isset($keyword))? array('keyword'=>$keyword) : array();
		$cat = array_merge($keyparam,$catparam);
		$apicall = self::setClient($operation,$cat,$position);
		$resp = self::getClient($apicall);
		$resp = json_decode(json_encode($resp));
		if(isset($resp->searchResult->item)){
			$resp = $resp->searchResult->item;
		//} elseif($resp->ack != 'Success') {
		} else {
			print('Caught getItems exception eBay failed '.$operation.' keyword: '.$keyword);
			return FALSE;
		}
		return $resp;
	}

	/** getItem returns all item data of one specified item ID 	**/
	// REFACTOR - Similar to Amazon.  Move to parent or no?
	public function getItem($var) {
		if(is_numeric($var)){
			$apicall = self::setClient('find',$var);
			$resp = self::getClient($apicall);
			$resp = json_decode(json_encode($resp))->Item;
			return $resp;
		} else {
			return false;
		}
	}

	/** getShipping retrieves shipping information for product ID = $var **/
	public function getShipping($var, array $options){
		$apicall = self::setClient('ship',$var,NULL,$options);
		$resp = self::getClient($apicall);
		return $resp;
	}

}
?>
