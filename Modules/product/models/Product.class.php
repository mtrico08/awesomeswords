<?php
namespace Modules\Product\Models;

use \Core\Models\Module as Modules;

use \Modules\Amazon\Models\Amazon as Amazon;
use \Modules\Ebay\Models\Ebay as Ebay;

abstract class Product extends Modules {
	public $helper;

	public function __construct() {
		$this->helper = self::getHelper();
	}

	abstract public function setVars();
	abstract protected function setClient($operation, $cat, $position, $var, $sort);
	/*
		$Operation = API operation
		$cat = First param (ie. Category)
		$position = Pagination number or maximum return limit, dependent upon the API
		$var = Second param (ie. Keyword). Also may permit pagination and sorting
		$sort = Sort type.  Sort cannot be used if 2 params aren't used.
	*/
	abstract public function getClient($apicall, $var);
	abstract public function getCategories($id);
	abstract public function setCategories($var);
	abstract public function setFindByCat($var,$cat);
	abstract public function getRef($index,$type,$var);
	abstract public function getItems($var,$operation,$position,$keyword);
	abstract public function getItem($var);
	abstract public function setPagination($pos,$count);

	/** getHelper is a method to reach the helper class **/

	public function getHelper(){
		return new \Core\Models\Helper;
	}
	
}

?>
