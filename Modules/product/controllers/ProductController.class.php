<?php
namespace Modules\Product\Controllers;

use \Modules\Product\Models\Product as Product;

class ProductController {

	private $objects;
	private $results_array;
	private $sub_page;
	private $page;
	private $total_count;
	private $items;
	private $length;
	private $start;
	private $apis;
	private $api_count;
	private $helper;

	/** Constructor property setters:
	/*	Sub_page returns the position of a sub-page within a total page of results.
	/*	For instance, 100 results on a "page" can be broken down into 10 subpages at 10 each.
	/*	The page variable is an API pagination param, so page 2 will therefore return results 200-300.
	**/
	public function  __construct(){

		/** Include the apis you'd like to use, here.  Class names and namespacing must match **/
		$apis = array('ebay','amazon');
		/** end your api's **/

		// REFACTOR: There's probably a better way of getting the helper.
		$this->helper = new \Core\Models\Helper;
		$this->objects = (object)[]; // "new" fails w/ sp_autoload (no ArrayObject/StdClass)
		$this->items = array();
		$this->sub_page = $this->total_count = 0;
		$this->page = 1;
		$this->results_array = array();
		$this->apis = array();
		foreach($apis as $api){
			if($this->helper->validateURL($api,MOD)){
				$this->apis[] = $api;
			}
		}
		$this->api_count = count($this->apis); // Count of sources we're calling upon. Amazon + Ebay = 2.
		foreach($this->apis as $api){
			try	{
				$namespace = '\\Modules\\'.ucfirst($api).'\\Controllers\\'.ucfirst($api).'Controller';
				$this->objects->$api = new $namespace;
			} catch (Exception $e) {
				print($api.' namespace problem. Caught exception: '.$e->getMessage()."\n");
			}
		}
	}

	/**
	/*	setCache triggers cache build
	**/
	public function setCache($type,$var){
		foreach($this->apis as $api){
			$this->objects->$api->setCache($type,$var);
		}
		return true;
	}

	/**
	/*	buildItems
	/*	Returns the results and tallies the count then sorts by relevance.  Also combines multi-word phrases for universally clean search.
	**/
	public function buildItems($index,$type,$var){
		$var = preg_replace('/\s+/', '+', $var);
		$items = array();
		foreach($this->apis as $api) {
			$items = array_merge($items,$this->objects->$api->getItemsFromRef($index,$type,$var,$this->length,$this->page));
		}
		foreach ($items as $key=>$item) {
			// Evaluates and weighs ranks $index (ie. key of "name") to match $var, then sorts by weight.
			$item->rank = ($rank = $this->helper->getCompare($var,$item->$index))? $rank : 50 ;
			$this->items[$key] = $item;
		}
		$this->total_count = count($this->items);
		$this->items = array_unique($this->items, SORT_REGULAR);
		return usort($this->items, array($this,"rankSort"));
	}

	/**
	/*	rankSort
	/*	Sorts by rank
	**/
	public function rankSort($a, $b){
		if($b->rank == $a->rank){ return 0 ; }
		return ($b->rank < $a->rank) ? -1 : 1;
	}

	/**
	/*	sliceItems
	/*	Limits the output of returned results, for pagination.  array_slice=TRUE param preserves keys.
	**/
	public function sliceItems(){
		$result = array_slice($this->items,$this->start,($this->length*$this->api_count),TRUE);
		return $result;
	}

	/**
	/*	getPageBuild
	/*	Determines the action to take given the pagination settings.  Also fires the building and slicing accordingly.
	**/
	public function getPageBuild($index,$type,$var){
		if($this->sub_page!=0){
			// Build a new object, slice, and iterate. Or skip and slice on return.
			if($this->total_count==0){
				self::buildItems($index,$type,$var);
			}
			$results = self::sliceItems();
			// If the total resuts are less than or equal to maximum number of all results up to this point, start over.
			if($this->total_count <= ($this->length*$this->api_count*$this->sub_page)){
				$this->sub_page = 0; // Reset sub_pagination.
				$this->page++; // Iterate to next page.
			}
			return $results;
		} else {
			// Reset pagination.
			$this->sub_page = 0;
			// Amazon pagination is 10 per page, maximum 10 pages.
			if(($this->start>0)&&($this->length>$this->length)){
				// REFACTOR: How to deal with non-standard pagination like 5-15.
				$this->page = (isset($this->page))? $this->page : ceil(($this->start+$this->length)/$this->length);
			} else {
				$this->page = 1;
			}
			self::buildItems($index,$type,$var);
			$results = self::sliceItems();
			return $results;
		}
	}

	/** setPaginate
	/*	Sets up the pagination rules for getters and setters.
	**/
	public function setPaginate($limit=20,$page=NULL){
		// Validate proper input and then, only if the subpage has been reset, use the page parameter.
		if(isset($page)&&is_numeric($page)){
			$this->sub_page = ($page-1);	// subpage starts at 0, because it initiates the "start" position.
		}
		$this->length = floor($limit/$this->api_count); // Evenly limiting merged results go 1/x of limit.
		$this->start = ($this->sub_page)*$limit;  // Start position of array_slice
		return true;
	}

	/** nextPaginate
	/*	Iterates to the next item.
	**/
	public function nextPaginate(){
		return $this->sub_page++;
	}

	/**
	/*	setItemsByCategory
	/*	Returns all item ID's from each eBay and Amazon, which belong to a category $var.
	/*	Looks up all item details for each item ID on each eBay and Amazon and merges them into a single araray.
	**/
	public function setItemsByCategory($var){
		return self::getPageBuild('name','findByCat',$var);
	}

	/**
	/*	getItemsByCategory
	/*	Outputs partial of all single item partials.  Default limits to 20 results per page.
	/*
	/* 	To use pagination in a view file, call the same object's method several times with a limiter.
	/*	The function will automatically paginate upon each call.
	/*	Specifying a page in the 3rd parameter will explicitly call that sub_page.
	/*	You may jump ahead to any subpage of results but jumping ahead several pages past
	/*	maximum will simply start at position 1 of the next parent page.
	/*	"start" is position in array, $this->length is length of results  (not position).
	**/
	public function getItemsByCategory($var,$limit=20,$page=NULL){
		self::setPaginate($limit,$page);	// Sets pagination.  Necessary for pagination
		$results = self::setItemsByCategory($var);
		self::nextPaginate();	// Paginates.  Necessary for pagination
		return $results;
	}

	/**
	/*	showItemsByCategory
	/*	Echoes out the formatted items. Pagination is the same as getItemsByCategory.
	**/
	public function showItemsByCategory($var,$limit=20,$page=NULL){
		$results = self::formatItemsByCategory($var,$limit,$page);
		echo($results);
		return true;
	}

	/**
	/*	formatItemsByCategory
	/*	Formats the items within a loop. Pagination is the same as getItemsByCategory.
	**/
	public function formatItemsByCategory($var,$limit=20,$page=NULL){
		$i = 0;
		$results = '';
		foreach(self::getItemsByCategory($var,$limit,$page) as $key=>$item){
			$item->i = $i++;
			$results .= self::getItem($key,$item);
		}
		return $results;
	}

	/**
	/*	getItem
	/*	Outputs partial of single item. Requires Key and Item (ie. foreach($items as $key=>$item)
	**/
	public function getItem($key,$item){
		// REFACTOR 0: Is there a way to incorporate partial loading for modules into the module controller, itself?
		if(null !== ($partial = $this->helper->getModPart('product','item'))){
			$string = $this->helper->requireToVar($partial,$item);
		} elseif(file_exists(PARTIAL.'item.'.EXT)) {
			$string = $this->helper->requireToVar(PARTIAL.'item.'.EXT,$item);
		} else {
			$string = '<article id="'.$key.'" class="item" data-cat="'.$item->cat.'" data-source="'.$item->source.'">
						<a href="'.$item->url.'">
							<div class="photo">
								<img src="'.$item->img.'" />
							</div>
							<div class="info">
								<h1 class="name">'.$item->name.'</h1>
								<div class="price">'.$item->price.'</div>
							</div>
						</a>
					</article>';
		}
		return $string;
	}

}

?>
