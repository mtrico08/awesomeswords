<?php
namespace Modules\Amazon\Models;

use Modules\Product\Models\Product as Product;

class Amazon extends Product {
	private $aws_access_key_id;
	private $aws_secret_key;
	private $endpoint;
	private $uri;
	private $timestamp;
	private $success;
	private $locale;
	private $browse_nodes;
	private $array_results;
	private $id;
	private $max_entries;
	private $position;
	private $end_position;
	/** Must remain public **/
	public $operations;
	public $helper;
	
	public function __construct(){
		$this->helper = parent::getHelper();
		return self::setVars();
	}

	public function setVars($locale='US'){		// Locale Null would work, too
		// REFACTOR 0: All of these vars should be in the controller, passed from the constructor.
		// AWS Access Key ID, as taken from the AWS Your Account page
		$this->aws_access_key_id = "AKIAIKLMGS27MGXEVCYQ";
		// AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$this->aws_secret_key = "YYoZ3Y6QNxkROTy6iMyCrictCVIKNcUmiL2mxUuu";
		// AWS application name
		//$this->associate_tag = "awesomeswor01-20";
		$this->associate_tag = "dadstie20-20";
		// Regional Endpoint
		$this->endpoint = "webservices.amazon.com";
		$this->uri = "/onca/xml";
		$this->array_results = array();
		$this->array_temp = array();
		// These are defaults.  We'll override them later.
		$this->per_page = 10;
		$this->max_entries = 50;
		$this->position = 1;
		$this->end_position = ceil(abs($this->max_entries)/abs($this->per_page));
		$locales = array('BR','CA','CN','DE','ES','FR','IN','IT','JP','MX','UK','US');
		if(isset($locale)&&in_array($locale,$locales)){
			$this->locale = $locale;
		} else {
			$this->locale = 'US';
		}
		$this->operations = array('categories','find','findbycat');
		
		// Contains Search Index and Browse Node for each and all root level categories
		$this->browse_nodes = array(	
										'UnboxVideo'=>'2858778011',
										'Appliances'=>'2619526011',
										'MobileApps'=>'2350150011',
										'ArtsAndCrafts'=>'2617942011',
										'Automotive'=>'	15690151',
										'Baby'=>'165797011',
										'Beauty'=>'11055981',
										'Books'=>'1000',
										'Music'=>'301668',
										'Wireless'=>'2335753011',
										'Fashion'=>'7141124011',
										'FashionBaby'=>'7147444011',
										'FashionBoys'=>'7147443011',
										'FashionGirls'=>'7147442011',
										'FashionMen'=>'7147441011',
										'FashionWomen'=>'7147440011',
										'Collectibles'=>'4991426011',
										'PCHardware'=>'541966',
										'MP3Downloads'=>'624868011',
										'Electronics'=>'493964',
										'GiftCards'=>'2864120011',
										'Grocery'=>'16310211',
										'HealthPersonalCare'=>'3760931',		// Threw API failure once.
										'HomeGarden'=>'1063498',
										'Industrial'=>'16310161',
										'KindleStore'=>'133141011',
										'Luggage'=>'9479199011',
										'Magazines'=>'599872',
										'Movies'=>'2625374011',
										'MusicalInstruments'=>'11965861',
										'OfficeProducts'=>'1084128',
										'LawnAndGarden'=>'3238155011',
										'PetSupplies'=>'2619534011',
										'Software'=>'409488',				// Threw API failure once.
										'SportingGoods'=>'3375301',
										'Tools'=>'468240',
										'Toys'=>'165795011',
										'VideoGames'=>'11846801',
										'Wine'=>'2983386011',
										//'Pantry'=>'N/A',
									);
	}

	/** setClient builds the API client by Operation, Category, and ID
	**/
	protected function setClient($operation, $cat = 'all', $position = NULL, $var = NULL, $sort = NULL){
		if(!in_array(strtolower($operation),$this->operations)){
			throw new \Exception('Incorrect API Search Type.');
		}
		
		$tracking = array(	"Service" => "AWSECommerceService",
							"AWSAccessKeyId" => $this->aws_access_key_id,
							"AssociateTag" => $this->associate_tag);
		
		if($operation == 'find'){
			// Single Item lookup via ID. Cat is Var in this case, as only 2 params needed in SetClient
			$query = 'ItemLookup'; 
			$response = "ItemIds,Images,ItemAttributes";
			$params = array(
							'ItemId' => $cat,
							'ResponseGroup' => $response
							);
		} elseif($operation == 'categories'){
			// Returns parent and children categories belonging to category of ID = $cat.
			$query = 'BrowseNodeLookup';
			$params = array(
							'BrowseNodeId'=>$cat,
							);
		} elseif($operation == 'findByCat'){
			// Keyword search plus. Cat may be a single string of Search Index or Single pair (array) of searchindex to browsenode.
			if(is_array($cat)){
				reset($cat);
				$index = key($cat);
				$max_total = (strtolower($index) != strtolower('all'))? 5 : 10; // If SearchIndes is "All", we can only go up to 5.
				$this->end_position = min($max_total,$this->end_position); // We'll take the smaller of either the existing max entry or the ceiling
				$sort = ($sort=NULL)? 'relevance' : $sort; // Refactor 1: Change this to salesrank once our algorithm is stronger.
				$cats = array(
							'SearchIndex' => $index,
							'BrowseNode' => $cat[$index],
							'Sort' => $sort,
							);
			} else {
				if(strtolower($cat)!=='all'){
					$cat = 'All';
				}
				$max_total = ($cat != 'All')? 10 : 5; // If SearchIndex is "All", we can only go up to 5.
				$this->end_position = min($max_total,$this->end_position); // We'll take the smaller of either the existing max entry or the artificial ceiling
				$sort = ($sort=NULL)? 'relevance' : $sort;
				$cats['Sort'] = $sort;
				$cats = array('SearchIndex' => $cat);
			}
			$query = 'ItemSearch';
			//$response = "ItemIds,Images,ItemAttributes,EditorialReview,OfferSummary,Reviews";
			$response = "ItemIds,Images,ItemAttributes,Reviews";
			if(isset($var)){
				$cats = array_merge($cats,array('Keywords' => $var));
			}
			$params = array_merge($cats,array('ResponseGroup' => $response));
		}
		
		/** No else statement needed for params, as long as it sits within the operations conditionals, given that we validate the operation within the array above **/
		$params = array_merge(array("Operation" => $query),$params);
		// Amazon permits a maximum of 10 pages of 10 item results.  If search index is "all", it will only permit 5.
		if(isset($position)&&($position<=$this->end_position)){
			$params["ItemPage"]=$position;
		}
		// Set current timestamp
		$this->timestamp = $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		
		$params = array_merge($tracking,$params);
		// Sort the parameters by key
		ksort($params);
		$pairs = array();
		foreach ($params as $key => $value) {
			array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}
		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);
		// Generate the string to be signed
		$string_to_sign = "GET\n".$this->endpoint."\n".$this->uri."\n".$canonical_query_string;
		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->aws_secret_key, true));
		// Generate the signed URL
		$request_url = 'http://'.$this->endpoint.$this->uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);
		return $request_url;
	}

	/** getClient validates and retrieves client curl address. Returns as XMLElement Objects
	/*	Here our response expects only one other object before the details we want, so we can
	/*	just unset the OperationRequest response instead of iterating through.
	/*	"Var" is a searchable index
	**/
	public function getClient($apicall, $var=NULL){
		usleep(1005000);	// Amazon throttles 1 call per sec
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apicall);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$xml = curl_exec($ch);
		if(curl_exec($ch) === false)
			{ echo 'Curl error: ' . curl_error($ch); }
		$resp = simplexml_load_string($xml);
		//$resp = simplexml_load_file($apicall);
		$valid = self::verifyRequest($resp);
		if(FALSE !== $valid && isset($this->timestamp)){
			unset($resp->OperationRequest);
			$results = $resp;
			if(isset($var)){
				$results = $this->helper->recursiveIterateKeyMatch($resp,$var);
			}
			return $results;
		} else {
			print($apicall); die();
			throw new \Exception('API XML load failure');
		}
	}

	/**
	 *	verifyRequest function sets a "success" and "timestamp" value and returns as true or false.
	**/
	public function verifyRequest($xml){
		if(isset($xml)){
			if(isset($xml->Items->Request->IsValid)){
				$this->success = $xml->Items->Request->IsValid;	
			} else {
				unset($xml->OperationRequest);
				if(isset($xml)){
					foreach($xml as $key=>$block){
						if (strtolower($key)==strtolower('IsValid')){
								$this->success = $block;
								break;
						} else if(count((array)$block)>0){
								self::verifyRequest($block);
						}
					}
				}
			}
		}
		if($this->success==1||strtolower($this->success)==strtolower('True')){
			return TRUE;
		} else {
			// Sometimes valid ap calls get throttled... which is annoying.
			$this->success = 'False';
			return FALSE;
		}
	}

	/** 
	 *	getCategories
	 *	Returns specified Category and its children
	**/	
	public function getCategories($id) {
		$apicall = self::setClient('categories',$id);
		try{
			$resp = self::getClient($apicall,'BrowseNode');	// 2nd param is an index to match.
		} catch(\Exception $e) {
			print('Caught exception: '.$e->getMessage()."\n");
			return FALSE;
		}
		return $resp;
	}

	/** setCategories runs through the browse_nodes array to build the whole category list 
	/*	with setChildrenCat. REFACTOR: alter this to run each lookup on separate times
	**/
	public function setCategories($var='all'){
		if($var=='-1'||strtolower($var)=='all'||is_array($var)){
			$results = array();
			$group = (!is_array($var))? $this->browse_nodes : $var;
			foreach($group as $index=>$nodes){
				$results = self::setChildrenCat($nodes,$index);
			}
		} else {
			$results = self::setChildrenCat($var);
		}
		return $results;
	}

	/**
	 *	setCategories returns all categories entirely
	**/
	public function setChildrenCat($var=NULL,$index=NULL) {
		$this->id = (isset($var)) ? $var : $this->id;
		// Amazon's API doesn't like super fast and frequent calls, we'll alwasy wait another 1 for safe measure.
		// The default limit is 3,600 per hour (1 per second).
		$children = self::getCategories($this->id);
		
		if($children){
			foreach($children as $key=>$child){
				$this->id = (string)$child->BrowseNodeId;
				if (false !== array_search($this->id, array_column($this->array_results, 'id'))){ continue; } else {
					$name = $child->Name;	// Name and Index seem to be different... look into that.  REFACTOR 1
					$parent = ($index)? $index : $child->Ancestors->BrowseNode->BrowseNodeId;
					$this->array_results[] = array('id'=>$this->id,'name'=>(string)$name,'parent'=>(string)$parent,'source'=>'amazon');
				}
				if(($child->Children)&&(count((array)$child->Children)>0)){
					foreach($child->Children->BrowseNode as $lowest){
						self::setChildrenCat($lowest->BrowseNodeId,$index);
					}
				}
			}
		}
		return $this->array_results;
	}
	
	/** 
	 *	setPagination
	 *	Sets the number of results per page and the page that we're working on.
	 *	Unlike eBay, Amazon just returns multiples of 10, up to 10 (10, 20, 30, etc).  Our pagination is based on our count, though, so calculations are needed.
	 *	ie. page:1,count:25 means we start on page 1 and end on page 2.5(round up to 3).  page:2,count:25 means we start at 2.5(round up to 3) and end end of page 5
	**/
	public function setPagination($pos,$count){
		$start_page = ceil((((abs($pos)-1)*abs($count))+1)/$this->per_page);
		$end_page = ceil((abs($pos)*abs($count))/$this->per_page);
		$this->position = ($start_page>10)? 10 : $start_page;
		$this->end_position = ($end_page>10)? 10 : $end_page;
		$this->max_entries = $count;
		// End Position can't go over 5 if no SearchIndex is selected and can't go over 10 at all.
		// We'll analyze that in the setClient method when we make the call.
		return true;
	}

	/** 
	 *	setFindByCat 
	 *	performs a keyword lookup across all Indexes or parameter defined "$cat" array (cat name => id);
	 *	Also, pass "position" as 3rd param of getFindByCat, to get only 1 page of results. REFACTOR... ?
	**/
	public function setFindByCat($var,$cat=NULL){
		if(isset($cat)&&(strtolower($cat)!=strtolower('All'))){
			$cat = is_array($cat)? $cat : array($cat);
			foreach($cat as $index=>$id){
				if(is_array($id)){
					foreach($id as $value){
						// This is all weighted by relevance, so we only need the first few results of each category.
						$results[$index] = array_merge($results[$index],self::getItems(array($index=>$value),'findByCat',NULL,$var));
					}
				} else {
					// This is all weighted by relevance, so we only need the first few results of each category.
					$results[$index] = self::getItems(array($index=>$id),'findByCat',NULL,$var);
				}
			}
		} else {
			// First we do a keyword search across all categories.  We cap this on the lower of either our max ($count) or Amazon's max (50).
			$results['all'] = self::getItems('All','findByCat',NULL,$var);
			$cnt = count($results['all']);
			// If our desired count exceeds the number of results, we'll expand results with specific matching categories.  If there are any relevant category matches, we pass them as a param in weighted order.	
			if($this->max_entries>abs($cnt)){
				// Now we get all matching categories.
				$matching_cats = self::getRef('name','categories',$var);
				if(isset($matching_cats)&&!empty($matching_cats)){
					//$this->max_entries = 100;
					foreach($matching_cats as $index=>$ranked_match){
						if(is_array($ranked_match)){
							foreach($ranked_match as $key=>$val){
								$results[$key] = array();
								if(is_array($val)){
									foreach($val as $value){
										$results[$key] = array_merge($results[$key],self::getItems(array($key=>$value),'findByCat',NULL,$var));
										$cnt = $cnt+count($results[$key]);
										// Once we've retrieved the number or results we want, we stop looping.
										if($cnt>=$this->max_entries){
											continue 3;
										}
									}
								} else {
									$results[$key] = self::getItems(array($key=>$val),'findByCat',NULL,$var);
								}
							}
						}
					}
				}
			}
		}
		return (object)$results;
	}

	/** 
	/*	getRef gets ID's of type by index, sorted by weight from getCompare
	/* 	Accepts $type for search and data type, such as "categories".
	/*	Accepts $var for ID lookup.  Accepts $index for reference.
	**/
	// REFACTOR 0: getRef move to ProductModel or a controller?  Need to add a param for source, to pass to all those others
	public function getRef($index='name',$type='categories',$var='all',$position=1,$count=100){
		$array_results = array();
		if(strtolower($type)=='find'){
			// ItemLookup (by ID) REFACTOR 0
			$array_results = $this->helper->getData($type,$var,'Amazon',NULL,$position,$count,TRUE);
		} else if(strtolower($type)=='findbycat'){
			$array_results = $this->helper->getData($type,$var,'Amazon',NULL,$position,$count,TRUE);
		} else if(strtolower($type)=='categories'){
			$all = $this->helper->getData($type,'all','Amazon',NULL,NULL,NULL,TRUE);
			// BrowseNodeLookup: Returns a 1 level array of all types (ie. product names or categories) matching value in weighted order. Includes ID and Index.
			foreach ($all as $item) {
				// Evaluates and weight ranks $index (ie. key of "name") to match $var, then sorts by weight.
				if(FALSE!=($rank = $this->helper->getCompare($var,$item->$index))){
					$array_results[$rank][$item->parent][] = $item->id;
					krsort($array_results);
				}
			}
		} else if(strtolower($type)=='similar'){
			// SimilarityLokup (by ID) REFACTOR
		}
		return $array_results;
	}

	/**
	 *	getItems
	 *	Returns all items within a category by category name.  Handles any operation (ie categories, findByCat, find, AND adds "keyword" as a potential parameter)
	**/
	public function getItems($var,$operation='findByCat',$position=NULL,$keyword=NULL) {
		// If we've set the pagination, we'll run through results for all pages from start page (position) to end page (max_entries)
		$results = array();
		$position = max($position,$this->position); // We'll either use either of the set start pages, whichever is higher, as we expect that's intended.
		$str = is_array($var)? key($var) : $var;
		if(strtolower($str==strtolower('All'))&&($this->end_position>5)){ 
			$this->end_position = 5;
		} elseif($this->end_position>10){ 
			$this->end_position = 10;
		}
		for($i=$position;$i<=$this->end_position;$i++){
			$apicall = self::setClient($operation,$var,$i,$keyword);
			$resp = self::getClient($apicall);
			$resp = $resp->Items;
			if(!isset($resp->Request->Errors)){
				foreach($resp->Item as $item){
					$results[] = $item;
				}
			} elseif(null !== ($error = $resp->Request->Errors->Error->Code) && $error == 'AWS.ECommerceService.NoExactMatches') {
				// Do nothing... We'll bypass these
			} else {
				print( 'Caught getItems exception with'. $apicall);  // We'll report calls where there are syntax or parameter errors.
			}
		}
		return $results;
	}

	/**
	 *	getItem
	 *	Returns all item data of one specified item ID 
	**/
	public function getItem($var) {
		$apicall = self::setClient('find',$var);
		$resp = self::getClient($apicall);
		$resp = json_decode(json_encode($resp));
		return $resp;
	}

}
?>