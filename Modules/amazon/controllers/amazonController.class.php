<?php
namespace Modules\Amazon\Controllers;

use \Modules\Amazon\Models\Amazon as Amazon;

class AmazonController {
	
	public $amazon;
	public $array_results;
	public $helper;

	public function __construct(){
		$this->amazon = new Amazon;
		$this->array_results = array();
		$this->helper = $this->amazon->helper;
	}

	/** setCache triggers cache build **/

	public function setCache($type,$var){
		return $this->helper->setCache($type,$var,'Amazon');
	}

	/** getItemsFromRef performs a lookup for a value ($var) on all items matching a
	/*	specified index value, and organization type, such as "categories"
	/*	example: "get all Item data for $var=all $type=categories by $index=name"
	/*	example: "get all Item data for $var=all $type=items by $index=name" <-- REFACTOR
	/*	Runs relevant keyword searches, first and builds upon those results with category results
	/*	Accepts "extended" param: "basic" is fastest, "extended" includes more details
	/*	Accepts "position" to set the maximum number of results
	**/
	// REFACTOR 0 - Doesn't do anything with "extended", yet.

	public function getItemsFromRef($index='name',$type='categories',$var=NULL,$count=10,$position=1,$extended='basic'){
		// REFACTOR 0: Built for the future capability of taking type that matches operation;
		if(in_array($type,$this->amazon->operations)){
			$operation = $type;
		} elseif ($index=='id') {
			if(is_numeric($var)){
				$operation = $type = 'find';
			} else {
				throw new \Exception('ID must be numeric');
			}
		} else {
			$operation = 'findByCat';
		}
		$this->amazon->setPagination($position,$count);
		$array = $this->amazon->getRef($index,$type,$var,$position,$count);
		self::formatItems($array);
		if($type=='categories'){
			// REFACTOR:  Do we need any of this anymore 51-65?
			$keyword_results = $type_results = array(); // IE. "categories" results.
			// We ordered our array by weight, so we just need to drop that by diving 2 levels deep
			foreach($array as $ordered_block){
				// Each search index contains multiple categories, so we rebuild a searchindex=>category value pair
				// for each one, in the loop, and use those two values in the client build.
				foreach($ordered_block as $searchindex => $category){
					foreach($category as $value){
						// Returns results, matching category and keyword, just like FindByCat.  Do we need this, then?
						$keyword_results[] = $this->amazon->getItems(array($searchindex=>$value),$operation,$position,$var);
						// Returns results all results of ONLY matching categories, no keyword.  We do this to provide expanded results
						$type_results[] = $this->amazon->getItems(array($searchindex=>$value),$operation,$position);
					}
				}
			}
			self::formatItems(array_merge($keyword_results,$type_results));
		} else {
				// REFACTOR 0:  What do we do if not categories?
		}
		// REFACTOR 0: For load time, run keyword_results first and process remainder in bg?
		// REFACTOR 0: This will load more than 20 results, which screws with pagination.
		// REFACTOR 0: Using pagination screws with rankings of keyword and category groupings because page 2 keywords should outrank page 1 categories?
		return $this->array_results;
	}

	function formatItems($var){
		// If the response was loaded, parse it and build links
		foreach($var as $item) {
			if(is_object($item)&&isset($item->ASIN)) {
				$id = (string)$item->ASIN;
				if(array_key_exists($id,$this->array_results)){
					continue;
				}
				$img = (isset($item->LargeImage))? (string)$item->LargeImage->URL : "http://pics.amazonstatic.com/aw/pics/express/icons/iconPlaceholder_96x96.gif";
				$link  = (isset($item->DetailPageURL))? (string)$item->DetailPageURL : '';
				$title = (isset($item->ItemAttributes->Title))? (string)$item->ItemAttributes->Title : '';
				// REFACTOR - You have to run an item lookup to get the category.
				$cat = (isset($item->ItemAttributes->ProductGroup))? (string)$item->ItemAttributes->ProductGroup : '';
				
				// Start - Calculate price
				$price = (isset($item->ItemAttributes->ListPrice->Amount))? sprintf("%01.2f", $item->ItemAttributes->ListPrice->Amount) : ''; // Price is in cents... need to breal that down.
				$formattedprice = (isset($item->ItemAttributes->ListPrice->FormattedPrice))? $item->ItemAttributes->ListPrice->FormattedPrice : '';
				$currency = (isset($item->ItemAttributes->ListPrice->CurrencyCode))? (string)$item->ItemAttributes->ListPrice->CurrencyCode : '';
				// End - Calculate price
				
				$this->array_results[$id] = (object) array ('name'=>$title, 'url'=>$link, 'img'=>$img, 'price'=>$formattedprice, 'cat'=>$cat, 'source'=>'amazon');
			} elseif(is_array($item)||is_object($item)) {
				// If we're not at the right level, we'll recursively foreach until we get there
				self::formatItems($item);
			}
		}
		// Don't repeat results of a keyword and generic search
		return $this->array_results;
	}
}	
?>