<?php
namespace Customize\Config;

/** Administrator framework settings
/*  This is essentially a controller for data to the model.  It's intentionally a rudimentary constructor class, for ease of use by administrator 
*/

Class Settings {
	
	public $pages;
	public $css_order;
	public $js_order;
	public $activate_module;
	public $content_type;
	public $scripts;
	public $title;
	public $tagline;
	public $theme;

	public function __construct(){
		
		/** START CUSTOMIZE DO NOT EDIT, ABOVE. **/
		
		$data_settings = [
			'title' => '', 					// Enter your site's title, if none is present, we'll build it!
			'tagline' => 'Find Things', 	// Enter your site's tagline, if none is present, we'll build it!
			'theme' => 'theme',				// Enter your site's theme, if none is present, we'll assume it's "theme".
		];
		
		$asset_settings = [
			'css' => ['main', 'responsive'], 		// Order of preferred frontend assets auto-loaded in header and footer (CSS, JS)
			'js' => ['masonry','main','minify'], 	// Filename spelling must be correct.  Only prefixes are read in V0, so use unique prefixes.
		];
		
		/**
		/*	ALL files present in the directories will be loaded, this setting ONLY controls DOM order. only CSS and JS files will be loaded (not scss).  If you want a file to not be loaded, change the extension.
		/*	Invalid file suffixes or files which do not exist will be ignored.
		**/

		$pages = [
			// List sitemap for standard pages
			0=>'swords',
			'page' => [0=>'child'],	// This would also work as 'page' => 'child'
		];
		
		$modules = [	// Mark a module as activated
			'ebay',
			'amazon',
			'product',
		];
		
		$scripts = [
					'product_loader',
					'cacher',
					];
		
		$content = [
					'blog'=>['article']	// The blog is default included but you can disable it simply by removing it. The blog's array must contain at least 1 value.
					];
		// EXAMPLE Content Type Array:
		$content = [
			'blog' => ['test'],
			'content 1' => [
				'listing'=>[
					0=>'post 1',
					1=>['post 2'=>[
									0=>['craziness','new'],
									1=>'nonsense'
					]],
					2=>[
						'post 3'=>[
									0=>'small',
									1=>[0=>[
											'unncessary',
											'another',
											],
										1=>'test'
										],
									2=>'giant'
								]
					],
					3=>'post 4',
				],
			],
			'new list' => [0=>'sub',1=>'sub 1'],
			'post' => [
				'archive'=>['two'],
			],
			'test 3' => [
				'listing test'=>[
					0=>'testsub'
				]
			],
		];
		
		/** END CUSTOMIZE .  DO NOT EDIT, BELOW. **/
		$this->content_type = $content;
		$this->activate_module = $modules;
		$this->scripts = $scripts;
		$this->css_order = $asset_settings['css'];
		$this->js_order = $asset_settings['js'];
		$this->title = $data_settings['title'];
		$this->tagline = $data_settings['tagline'];
		$this->theme = $data_settings['theme'];
		$this->pages = array_merge(['home'], $pages); // This merge is done to allow the home template to be accepted.  Don't worry, we'll correct it up in the sitemap, later.
		return true;
	}
}

?>