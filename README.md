# README

Lean Framework is a lightweight OOP MVC PHP framework which simplifies the process of building and deploying modular applications.
The framework leverages auto-inclusion/auto-loading methods similar to Java, which eliminates the arduous process of building hooks and registration methods for frameworks like Drupal and WordPress.
---

# INSTALLATION

Installation is very simple, the framework only requires PHP and Apache, in order to run, so no additional middleware is necessary.  Simply set up the site in your local environment just as you normally would.
LeanFramework also works with SASS source files through Gulp, to compress and minify your source assets and copy them into the production directory.  You may use "Gulp" upon completion or "Gulp Watch" while editing, for realtime gulping.

## Gulp

Unlike the default LeanFramework, Gulp has several dependencies such as npm, ruby, gems, and compass.  To set those up, follow these instructions:

1. Get your free copy of NPM from https://nodejs.org/en/download/ and install it. NPM is used for running gulp
2. Get Ruby from https://rubyinstaller.org/ and install it. Ruby is used for Compass gem which is for compiling sass
3. Install ruby-sass gem through as shown here. Simply run "gem install sass"
4. At last run "npm install" command to install all required npm node modules.

Now, run "gulp" to compile everything and "gulp watch" for realtime compiling.

## Database

LeanFramework V0 (current) is database-less, so all of your custom data must either be entered in the settings file, described below, the view files themselves, or produced by a module or external API.
The V1 version which is currently in development, will accept both relational (MySQL) and NoSQL (MongoDB) data layers, so you may want to prepare your local environment accordingly.

## Settings

In order to complete installation, you must first fill out the basic customization settings within "customize/config/settings.class.php".  The file will contain commented instructions as follows:
### $data_settings.  This array contains website meta data.

1. 'title': Your default website Title.  This will become part of your page title just as any website would, such as "Page Title | Website Title".
2. 'tagline': Your website's descriptor/tagline.  We'll use this in page descriptions and default page titles, as well.  Page Titles are overwritten at the page level, which we'll discuss, later.
3. 'theme': By default, the framework comes with a theme called "theme" under the "/views" directory.  This will be loaded if the setting is left empty.  To create your own theme, just copy the "theme" directory with whatever name you want and then enter that name here.

### $asset_settings.  This array allows you to specify the order of assets loaded in the header and footer.  LeanFramework will auto-load all CSS and JS files, automatically, this setting only sets the order.  You can overwrite default auto-loading, which we'll discuss later on.

1. 'css': The order you'd like to load your css files.
2. 'js': The order you'd like to load your js files.

All files are automatically loaded, even if they're not listed here.  This just sets the priority.  Only the prefixes are read in V0, so it won't differentiate between "main.js" and "main.min.js", for instance.  It will simply grab whatever comes first.
V1 REFACTOR: This prefix issue will be resolved in V1.

### $pages, $modules, $content, $scripts:  

LeanFramework differentiates between 4 node-types: 'pages' which are stand-alone templated pages, 'content' which are lists of data sharing index/listing and details/landing templates (ie. Drupal content types or WordPres post types), 'scripts' which are executable URLs which load without DOM structure, and 'modules'.

1. $pages = This is your 'basic' page sitemap.  It allows for a nested hierarchy.  Any page which contains children should be listed as a key, while pages which have no children should have a numeric key, as seen in our demo settings.  The "home" page will be merged in after your settings, to properly match up with the home.php template.  This does create duplicate homes in the sitemap object ("/home", "/") but don't worry, we merge them into one (title:"home",url:"/"), during the getMenu method.
2. $content = This is your 'content' sitemap.  Content types allow for 4 sub-types:

* content type =  A content type is any array of data that can be traced back to 1 key in the root of the array (1st level/column).  The key of that array is inherited as the name of the content type.
* listing = A listing is the child of a content type and it also is a key, which simply identifies that all data within it, belongs to a shared listing.  Content types can have several sub-pages but only 1 listing and 1 archive.  We use listings in order to invoke the corresponding (or default) listing templates.
* archive = An archive is the same as a listing.  It sits in column 2, uses its key as its name, and serves as the second grouping of pages within a content type.  We use archives in order to invoke the corresponding (or default) archive templates.
* post = Also known as a landing/details page, a "post" is page which has no children.  It's the end of the line.  A listing may have many posts and posts may be mixed in with listings within a parent listing.

  In reality, any immediate child of a content type is a listing/archive but those would not match up with a listing or archive template.
  The nesting rules in content types are the same as they are for pages.
  
  The "blog" content type is default included in your settings.  To remove it, simply delete it from the array.

3. $modules = This array activates/registers your modules.  Previous versions of LeanFramework auto-loaded all modules but in this version, all modules are disabled until you list them here.  The name must be identical to the module's directory and namespaces, in order to be loaded.  If not, we'll ignore it.
4. $scripts = This array creates URLs which can be executed as scripts (ie. cron jobs, curl, ajax, etc).

That completes the settings file, let's review the core framework, next, and then move into themes.

# Core Framework

The framework is based on 3 "nodes", which are "pages", "content", and "modules", as seen above.  The index file will load the Factory class first, which is specially labeled with the "lib" file extension, to reduce unnecessarily overloading.  The Factory library then imports your custom settings, global constants, auto-loads classes, instantiates nodes, and calls upon validation and building methods from the helper class, to build and read URL's, match up templates with viable node types and urls, and dispatch corresponding objects to the render controller, which then outputs the view layer.

The framework will contain the following structure, which is explained, as follows:

* htaccess, index.php : These files are required to load the factory library.
* license.txt, readme.md : These files are instructional and licensing, only.
* cache : This directory stores temporary files (ie. JSON data from API scripts, etc).
* core : This directory serves all of the framework's major functions, constants, and helper methods.  You should never edit this directory.  All upgrades would run through this directory.
  * controllers : The controllers directory contains the factory library, which loads settings and all dependencies, the actions for the 3 node-types ("modules", "pages", and "content"), and the render controller which outputs the view files and provides you with the method to load modules.
  * models : The models directory contains the models for all 3 noe-types, all framework constants and settings which are read by the factory library, and the all important "Helper.class.php" file, which contains the helper methods which you'll use in your modules and templates.
  * gulpfile.js : The gulpfile.js file runs gulp for the view assets.  It sits in this directory so that we can control it as part of the core framework.
* customize/config : This directory contains the configuration settings file, discussed above.  All of your sitemap and global meta data settings are built here.
* log : This is your log directory.
* Modules : This directory is where you'll build all your modules.
  * Each subdirectory is a module's root.
    * Each module should have a "controller" and "model" module.  In V0, view partials remain in the "theme" but V1 (REFACTOR) may handle views within the modules, themselves.
* views : The views directory may contain several theme directories.
  * theme : "theme" is the default directory but LeanFramework will load whichever framework you've specified in your settings.
    * partials : Partials are reusable components like headers, footers, and widgets.  You should use partials for modules and includes.
	* pub : The pub directory contains your live assets.  You can either write directly to this or use the "src" directory with Gulp, to generate compressed and minfied assets, here.  The directories will look similar to "src" except that they're production files.  For more information, review the "src" directory info, below.
	* src : The src directory contains your source assets.  You can either use this to store your source media, as they won't be loaded, or you can store source assets like sass files, to use Gulp for minifying and compressing these files into production assets.  Those assets will automatically copy over to the "pub" directory.
      * css : Your sass files go here. You can also keep "css/mixins" and "css/webfonts" directories or any other subdirectories that you'd like for sass.  By default the "main.scss" file is your master scss file which will generate a main.css file.
	  * img : Your images go here.
	  * js : Your javascript files go here.  The framework may assume that you have a main.js file, by default.
	  * sprites : The images that you use in sprites will go here. We'll use sass to combine them with gulp, and generate final sprites.
    * templates : The templates directory contains all of your layout/template files as follows:
	  * 404.php : This will handle all 404 requests.
	  * default.php : This is your default "page" template.
	  * default.single.php : This is your default "post" template.
	  * "*.archive.php" : All content types can have their own archive templates.  In our demo settings, for instance, "content 1" has an archive, so the "content_1.archive.php" file serves as that template.  Content type and page name spaces are replaced with hyphens in filenames.
	  * "*.single.php" : This is your content type's post template.
	  * "content_1.php" : Every content type uses its own name as its listing template.  So, in our demo, we used "content 1".  The listing template is therefore "content_1.php".
	  * "*.single.php" : This is your content type's post template.  In our demo, that would be "content_1.single.php".
	  * post.php : This is your default listing template.  Listing templates are the only ones where the word "listing" is optional.  You can either choose to include it or not.
	  * post.archive.php : This is your default "archive" template.
	  * post.listing.php : This is your default "listing" template, it is the same as "post.php".
	  * "*.php" All "pages" and "modules" have their own templates which will match their names in settings.  For instance "home.php" is the home template and the "/" tmplate.  "cacher.php" is a template that serves a caching script.  These are fully custom php files, so you can use them however you'd like.

## Models

All models will appear as [name].class.php.  All *.class.php files will be auto-loaded from the core and any activated modules.
All models and controllers must have file and directory names which match their namespace.  Core models, for instance, sit at "core/name.class.php" and are part of the "\Core\Models" namespace, with classname "Name".

All core node models will implement the abstract node model.

You may see that most models will call upon the Factory or Helper classes.  This may be to access static methods or helper object methods.  You should focus on module model use, which we'll discuss later.

## Controllers

All controllers will appear as [name]Controller.class.php.  Again, all *.class.php files will be auto-loaded from the core and any activated modules.  Spelling, as always, is important.
All models and controllers must have file and directory names which match their namespace.  Core controllers, for instance, sit at "core/nameController.class.php" and are part of the "\Core\Controllers" namespace, with classname "NameController".

All core controllers will use a the Helper method and may include other "uses" to simplify namespacing between controllers and models.


# Modules

* Modules : This directory is where you'll build all your modules.
  * Each subdirectory is a module's root.
    * Each module should have a "controller" and "model" module.  In V0, view partials remain in the "theme" but V1 (REFACTOR) may handle views within the modules, themselves.

A module is a custom soup-to-nuts function.  While it is best practice to have a model and controller, a module can theoretically run without a model.  What is minimally required is the following:
1. The module's directory name and settings "activate" name must be the same.
2. The module must have a controller with the correct naming convention "[name]Controller.class.php" with a matching gclass name, parent directory, and namespace.

You may have to use the helper methods, so to do so, simply "return new \Core\Models\Helper;" or use the helper method connector that many default modules have built in.

To load a module, simply use the "getModule($arg)" function with either "self::" or "$this->", where $arg is your desired module.  If no argument is passed, it will load whichever module is associated with the template's URL. The Factory library will use helper methods to validate which objects to dispatch, so it will know where your module actually belongs!

The module must be activated, in order to run.

# Widgets

Widgets may be built in the "partials" directory.  Partials are simple include files and calling them is really easy.  Simply use "$this->partial($arg);" where $arg is the filename/prefix of your partial.

In V1 (REFACTOR), the partial method might be somewhat different and we hope to have easier methods for calling upon module and settings data from a partial.

# Sitemap

The sitemap is controlled using a few helper methods and may be called using "Helper::getMenu();".  The menu.php partial, which is the default for navigation, has this built into it and may be adjusted.  This will load URL's and titles built by the array you set up during settings.

----------------
# Layout

The rendering engine controller belongs to the core directory and instructs a number of methods from the settings file, located in /customize/config/settings.class.php

## Partials, Headers, Footers, and JS/CSS inclusion

As discussed under "widgets" and in the core directory structure, above, the partials directory contains simple include files such as the header and footer.

In the absence of a required partial (ie. header and footer), the engine will automatically assemble a header and a footer by auto-loading the files in your pub asset directories (ie. css and js) with the basic DOM structure.  The settings file permits you to choose the order in which these files load, via the "js" and "css" settings.

The body's layout file should not include the header and footer as those are handled by partials in the "views/partials" directory.  Partials may also be used to create other global includes such as sidebars and navigation.  To create a partial, simply create a filename in that corresponding directory.  Partials may be rendered by passing the filename (without the extension) to "partial('filename')" through either "self::" or "$this->".

You may also choose to modify the engine's auto-header and auto-footer features, to include other references or CDN's by creating an extension file.  Extension files are a partial within a partial, labeled in the same fashion as partials but with the "inc.php" extension.  "Header.inc.php", for instance, will automatically load in the bottom half of the <head> tag, after auto-loading has completed.  Extensions may be rendered by passing the filename (without the extension) to $this->extension('filename').

### Objects within partials

Passing an object from a template to a partial is simple.  Just pass the object as the second argument (ie. $this->partial('part',$object);).

## Body Templates

The body is assembled by files in the "views/templates" directory.  The files listed in the core section, above, are required for every project.  The settings class file (/customize/config/) includes a sitemap property which may be modified to indicate all valid page URL's.  Any page missing will return a 404, the homepage is already configured.

To create a template file, simple create a filename.php file where "filename" is the name of the URL which this file belongs for page nodes and modules.  For content type nodes, follow the directions from the core, above (*.listing.php, *.archive.php, *.single.php. etc).

For instance "domain.com/tester" would have filename "tester.php".  No body wrapper tags are necessary at the template level, simply the contents of the body would suffice.  Basic DOM elements such as head, body, and footer tags are located in the corresponding partials.

In the event an identified URL has no corresponding filename, it will assume the "default" file.

-------------------
# Scripts

Scripts are executable URL's which allow you to write OOP or procedural code for things like curl, ajax, cron jobs, etc.

If you're writing a stand-alone script (ie. to run on a cron job) to accompany a module or object, you must instantiate a module within the script, using the full namespace.
If you are invoking the script from a template, though (ie. using ajax calls), you can pass an object to the script, similar to partials.  In order to do this, simply use the RenderController's "getScripts" method (ie. self::getScripts('name')).  As with all templates, invoking a RenderController method can be done with "$this->" or "self::".  To pass the object to it, simply pass it as the 2nd argument, just like partials.

To register a script, follow these steps:
1. Add a value to the settings file scripts array.
2. Add a file to the scripts directory (/scripts).  Give the file a name that matches the value from step 1.

There are a few ways to pass parameter to a script:
1. The traditional ?[name]=[value] URL param will be read and exploded between each "&" by the Factory library, through the Helper method.
2. Pass the params as the 2nd argument in the RenderController's getScripts method (the 2nd arg is the object to analyze)
3. Pass the params as $_POST form values.

-------------------
# Nodes

To register a new node, follow these steps:
1. Add an array of node URL's to the settings file.  Be sure that you also pass it as a public property of the settings  class.
2. In the core's settings model, return the new property from the settings file.  For example, duplicate and edit the getContent function for the new property as needed.  Be sure that you're cleaning the value before returning it.  If you're using a nested array like the content node, be sure to pass the getUrlKey function.
3. Create corresponding directory for your template or executable file, within your theme (ie. /[node]).
4. Add a constant to the constants class (core/models/Constants.class.php) to define:
  1. Directory that you've created, if applicable, in step 3.
  2. Constants for logic and file naming conventions (you can basically just add define('NODE','node').  We don't like hard-coding file names and paths into code, so we do this to allow you to easily change the names and locations of files with only a change to this line instead of editing every line that references the old location.
5. Create a file with the same name in the core's models directory (core/models/[node].class.php).  Be sure that you're creating all methods from the parent node class if you're implementing the node class.
6. Edit the core controller's factory library.
  1. Add a protected property for the node.
  2. Return the method from step 2 as a property from step 6.1, within the constructor.
  3. Repeat step 6.2 as an array value within the getArray method.  Be sure to use the constant from step 4.2 as your key.
7. Add a core controller for the node to the core/controllers directory, of the name, such as core/controllers/[Node]Controller.class.php
8. Edit the render controller core/controllers/RenderController.class.php
  1. Add a protected property for the node.
  2. Add getters and setters for the node.
9. Edit the Factory library's objectDispatcher method to validate against your directory from step 3.

-------------------
# Product Module

The product module is a default included module, which loads from a variety of other modules.  It is very simple to use in templates.  Simply add your apis in the "this->apis" property of the ProductController.php file
Then, be sure that you have a module built with that corresponding name (ie. amazon or ebay) and add your keys in the top of the controller under setVars
Then, on whichever page you want to display products, simply add 2 variables for (a)the keyword you'd like to search and (b) the number of results per page, to display.

Pagination takes 2 optional arguments
1. how many results per page (default is 20 if you leave it blank).
2. which page to begin with (default is 1 if you leave it blank).

First, instantiate the module using self::getModule(), then echo the results with showItemsByCategory($var,$num), for each page, where "var" is the keyword and "num" is the results per page.

Read the notes above the pagination function for more information.  You can queue up the next page without specifying it, simply by calling the method a second time.

Use multiples of the number of apis you've set up, for best results. For example, if you have 3 apis that you're calling, set the page limit (number of results per page) to 6, 9, 12, 15, etc.  If you set the limit to 10 over 3 apis, it'll round down to 9 (3 each).  There are several templates already set up for you, such as the "swords.php" file.

## Structure

The structure of the product module is what we call a "sandwich module".  It is dependent upon the existence of other modules such as "ebay" and "amazon", because without them, there are no producst.
* Model:  The model is an abstract class for dependent modules; sort of like an interface (blueprint) for other Product Source modules, so you can keep adding new ones.  This is the bottom bread of the sandwich, where everything starts.  Each Source module extends the abstract class and builds upon it.
* Controller:  The controller is the end-point of the modules.  This is the top bread of the sandwich.  It combines the source modules into a single module.

If for whatever reason, you wanted to run these modules independent of one another, you would only have to (1) avoid extending the product model to each source module's model, (2) remove the corresponding sources from your product controller, (3) write your own actions.