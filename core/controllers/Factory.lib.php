<?php
namespace Core\Controllers;

use \Core\Models as Model;
use \Core\Models\Constants as Constants;
use \Core\Controllers as Controller;
use \Core\Controllers\RenderController as Render;
use \Customize\Config\Settings as Config;
use \Core\Models\Helper as Helper;

/**
/*	Factory Class
/*	This is the controller responsible for building the objects to pass to the render controller.
/*	It is instantiated by the index file, then auto-loads all classes, and uses helper methods to validate URL's and return and instantiate the curresponding node type.
**/

Class Factory {
	
	protected $url;
	public $type;
	protected $page;
	protected $content;
	protected $module;
	protected $scripts;
	protected $settings;
	protected $layout;
	protected $theme;
	
	public function __construct(){
	
		spl_autoload_extensions('.class.php');
		spl_autoload_register();
		
		self::init();
		$node = NULL;
		// Get node type and URL of current page.
		$template = self::objectDispatcher($this->url->url,$node);
		// Instantiate node type of current page, to return abstract META data to the rendering engine.
		$meta = (isset($template)&&($template!=NONE)) ? self::nodeDispatcher($this->url->url) : NULL;
		return $this->layout = new Render($this->url,$this->type,$template,$meta,$this->settings->getCss(),$this->settings->getJs());
		
	}
	
	/** 
	/*	init initializes settings
	**/
	public function init(){
		new Constants;
		// Load administrator settings
		$this->settings = new Model\Settings(new Config);
		// Sets editable theme setting as constant;
		$theme = isset($this->settings->config->theme)? $this->settings->config->theme : 'theme';
		Constants::setTheme($this->settings->config->theme);
		// Load helper methods for domain, url, title.
		$this->url = Helper::urlParser(NULL,NULL,$this->settings->getTitle(),$this->settings->getTagline());
		// Load page node
		$this->page = $this->settings->getPages();
		// Load content node
		$this->content = $this->settings->getContent();
		// Load module node
		$this->module = $this->settings->getModules();
		// Load scripts node
		$this->scripts = $this->settings->getScripts();
		// Load rendering controller
		return TRUE;
	}
	
	/** 
	/*	nodeArray
	/*	creates a array of viable node values per node type.
	*/
	public static function nodeArray($type=null){
		$settings = new Model\Settings(new Config);
		try{
			$nodeArray = array(	PAGE=>array_merge($settings->getPages(),array('')),
							CONTENT=>$settings->getContent(),
							MOD=>$settings->getModules(),
							SCRIPT=>$settings->getScripts(),
							);
			return (isset($type))? $nodeArray[$type] : $nodeArray;
		} catch(\Exception $e) {
			print('Caught exception: '.$e->getMessage()."\n");
		}
	}	
	
	/** 
	/*	objectDispatcher
	/*  Validates URL against all available nodes & returns node type.
	/*	Validates URL against all view files & returns template.
	/*	Passed to render controller to read a URL for directing which node model to validate against
	*/
	public function objectDispatcher($url){
		$result = '';
		foreach(self::nodeArray() as $type=>$node){
			// If we find the URL in the array of nodes, we return the key (node type), otherwise we return NONE (404)
			if(Helper::validateURL($url,$node)){
				$this->type = $type;
				// If the URL is valid, now we check to see if the view file is available.
				// This switch case is future proof, to allow for each node type to have its own directory.
				switch($this->type){
					case 'scripts':
						$dir = SCRIPTS;
						break;
					default:
						$dir = TEMPLATE;
				}
				return Helper::validateView($url,$type,$dir);
			}
		}
		return NONE;
	}
	
	/**
	/*	nodeDispatcher instantiates the node of a given URL and object's type.
	/*	returns meta data for rendering engine, within a single object.
	**/
	public function nodeDispatcher($url){
			$node = '\Core\Controllers\\'.ucfirst($this->type).'Controller';
			$node = new $node($url);
			
			$meta = (object)array('id'=>$node->getID(),'title'=>$node->getTitle(),'content'=>$node->getContent(),'hierarchy'=>$node->getHierarchy(),'url'=>$node->getURL());
			return $meta;
	}
	
}

?>