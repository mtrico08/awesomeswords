<?php
namespace Core\Controllers;

use \Core\Models\Helper as Helper;
use \Core\Models as Model;

Class ModuleController {
	
	public $model;
	public $modules;
	public $sitemap;
	public $url;
	public $parts;

	public function __construct($url=NULL){
		$this->model = new Model\Module;
		$this->modules = self::getAll();
		$this->sitemap = Helper::siteMap();
		$this->parts = (isset($url))? $url : '';
		$this->url = (is_array($this->parts))? implode('/',$this->parts): $this->parts;
	}
	
	/** 
	/*  getHierarchy method determines a module's URL's hierarchy (single, listing, archive).
	/*	REFACTOR 1: remove... this isn't necessary for a module, right?
	**/
	public function getHierarchy($parts=NULL){
			$parts = (isset($parts))? $parts : $this->parts;
			return $this->model->setHierarchy($parts);
	}
	
	/**
	/*	setAll stores all viable module nodes and pages from the Factory.
	**/
	public function getAll(){
		return $this->model->getAll();
	}
	
	/**
	/*	getURL returns the URL for the current page or a specified ID.
	/*	current page URL is sent by Factory lib upon being called.
	**/
	public function getURL($id=NULL){
		if(isset($id)){
			return $this->model->getURL($id,$this->sitemap);
		} else {
			return $this->url;
		}
	}
	
	/**
	/*	getID returns a unique numeric ID for a corresponding URL.
	/*	URL is generated during instantiation.  All nodes are pulled from Factory.lib.php siteMap function.
	**/
	public function getID($url=NULL){
		$url = (isset($url))? $url : $this->url;
		return $this->model->getID($url,$this->sitemap);
	}
	
	/**
	/*	getTitle returns the title for a corresponding ID
	/*	can be used for an object with given ID or a specified ID.
	**/
	// REFACTOR 0: Try to find the same ID in the nested array instead of reading from sanitized URL.
	public function getTitle($id=NULL){
		if(!isset($id)){
			end($this->parts);
		}
		return $this->model->getTitle($id,$this->sitemap);
	}
	
	
	/**
	/*	getContent returns the content for a corresponding ID
	/*	can be used for an object with given ID or a specified ID.
	**/
	public function getContent($id=NULL){
		// REFACTOR 0: Content should be in a separate array or JSON?
		return $this->model->getContent($id,$this->sitemap);
	}

}

?>