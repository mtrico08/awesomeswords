<?php
namespace Core\Controllers;

use \Core\Models\Helper as Helper;
use \Core\Models as Model;

Class RenderController {

	public $url;
	protected $node;
	public $template;
	public $title;
	public $domain;

	protected $head;
	protected $footer;
	protected $navigation;

	protected $css;
	protected $js;

	protected $page;
	protected $content;
	protected $scripts;
	protected $module;
	protected $obj;
	protected $params;

	/**
	/*	RenderController
	/*	Called by the Factory, which then validates URL and Node prior to instantiation, so no validation is necessary.
	/*	gets URL, template, node, css, and js arguments.
	**/
	public function __construct($url,$node,$template,$meta,$css,$js){
		$this->domain = $url->domain;
		$this->params = (object)$url->params;
		$this->url = (isset($meta->url)&&!empty($meta->url))? $meta->url : $url->url;	// REFACTOR 0:  The meta url is a string but the factory passed url is an array.  Does it even matter... I don't think we use this property, yet...
		end($url->url);
		$id = key($url->url);
		reset($url->url);
		$this->id = (isset($meta->id)&&!empty($meta->id))? $meta->id : $id;
		$this->title = (isset($meta->title)&&!empty($meta->title))? $meta->title : $url->title;
		$this->node = $node;
		// If parameters are passed by form submission, we store them here.
		if($_POST){
			$this->params = (object)array_merge((array)$this->params,$_POST);
		}
		if($node=='scripts'){
			return self::getScripts($this->id);
		} else {
			$this->css = $css;
			$this->js = $js;
			$this->head = self::partial(HEAD);
			$this->template = $template;
			$this->footer = self::partial(FOOTER);
			return self::layout($this->template);
		}
	}

	/** 
	/*	Extension Method
	/*	Finds partial extentions
	**/
	public function extension($arg){
		$result = '';
		foreach (glob(PARTIAL.$arg.".inc.".EXT) as $filename)
		{
			$result .= file_get_contents($filename);
		}
		return $result;
	}

	/** 
	/*	Alerts Method
	/*  Returns alert for a missing partial or view... if validation fails everywhere else
	**/
	public function alerts($arg){
		return 'Sorry, this '.$arg.' is either not loading or non-existent';
	}

	/** 
	/*	partial
	/*  echos out the partial or requires it.
	**/
	public function partial($arg,$obj=NULL){
		$output = '';
		if($arg==HEAD) {
			$output .= '<!DOCTYPE html><html lang="en"><head><title>'.$this->title.'</title>'. "\xA";
			$output .= '<link rel="shortcut icon" href="'.IMG.'favicon.ico" />';
			$output	.= self::settingsPreload('css'). "\xA";
			$output	.= self::extension(HEAD). "\xA"; // load the header extension file
			$output	.= self::extension(META). "\xA";
			$output	.= '</head><body>'. "\xA";
			echo($output);
		} elseif($arg==FOOTER) {
			$output .= '<footer>'. "\xA";
			$output .= self::extension(FOOTER). "\xA"; // load the footer extension file
			$output .= self::settingsPreload('js'). "\xA";
			$output .= '</footer></body></html>';
			echo($output);
		} elseif(false !== ($part = self::getPartial($arg,$obj))) {
			return include($part);
		} else {
			echo('<div class="error">'.self::alerts('partial').'</div>');
		}
	}
	
	/**
	/*	getPartial
	/*	Looks for partial files and returns them them.  Otherwise it will assemble a default partial by auto-loading the files
	/*	Permits passing objects back to the partial.
	**/
	public function getPartial($arg,$obj=NULL){
		$this->obj = $obj;
		if(file_exists(PARTIAL.$arg.'.'.EXT)){
			return PARTIAL.$arg.'.'.EXT;
		}
		return false;
	}
	

	/** Settings Preload Method
	/*	Auto-loads files for the partial method
	*/
	public function settingsPreload($arg,$contents=null){
		$result = '';
		$combined = array();
		if(is_null($this->$arg)||empty($this->$arg)){
			return $result;
		} else {
			if($arg=='css'){
				$open = "<link rel='stylesheet' type='text/css' ";
				$src = "href=";
				$close = " hreflang='en'>";
			} elseif($arg=='js'){
				$open = "<script type='text/javascript' ";
				$src = "src=";
				$close = "></script>";
			} else {
				$open = "<".$arg;
				$src = $contents." href=";
				$close = ">";
			}
			//  This function reads css and js files from the settings class and traverses the directories to validate existing file and valid filetype
			foreach ($this->$arg as $filename) {
					if(DEF!=(Helper::validateView($filename.'.'.$arg,'',constant(strtoupper($arg))))){
						$combined[] = $filename.'.'.strtolower($arg);
					}
			}
			$combined = array_unique(array_merge($combined,Helper::traverseDirFilename(constant(strtoupper($arg)),strtolower($arg))));

      // Check if has minified versions of file
      $has_minified = [];
      foreach ($combined as $filename) {
        if(strpos($filename,'.min.') !== false){
          $filename_array = explode('.',$filename);
          $has_minified[] = current($filename_array).'.'.end($filename_array);
        }
      }
      // Don't include extensions
      $dont_include = [
        'map'
      ];
			foreach ($combined as $filename){
        if (!empty($filename)) {
          $filename_array = explode('.',$filename);
          if (!in_array( end($filename_array) ,$dont_include ) && !in_array($filename,$has_minified)) {
            $consts = get_defined_constants();
            $dir = $consts[strtoupper($arg)];
            //$dir = VIEW.'pub/'.$arg;
            $result .= $open.$src."'".HTTPD.$dir.$filename."'".$close. "\xA";
          }
        }
			}
			return $result;
		}
	}

	/** 
	/*	Layout method
	/*  Renders the template and partials of a page
	**/
	public function layout($arg){
		echo($this->head);
		self::partial(NAVIGATION);
		if($arg==NONE){
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
		}
		require_once(TEMPLATE.$arg.'.'.EXT);
		echo($this->footer);
	}
	
	/**
	/*	setModule
	/*	If a module is called by the view file, we set it as this object's module property.
	**/
	public function setModule($arg=NULL){
		try{
			if(is_null($arg)&&($this->node)=='module'){
				$arg = $this->template;
			}
			if(Helper::validateURL($arg,MOD)){
				$module = '\Modules\\'.ucfirst($arg).'\Controllers\\'.ucfirst($arg).'Controller';
				$this->module = $module;
				return TRUE;
			}
		} catch(\Exception $e) {
			print('Invalid Module (module not specified, not enabled, and not default to this template: '.$e->getMessage()."\n");
			return FALSE;
		}	
	}

	/**
	/*	setScripts
	/*	If a script is called by the view file, we can allow an object and parameters to be sent to it.  Parameters can be in object, array, or string format.
	/*	REFACTOR 0:  Should this belong to the script node itself or the render controller?  I think this is good, 'cause post data goes through its constructor.
	**/
	public function setScripts($arg,$obj=NULL,$params=NULL){
		try{
			if(Helper::validateURL($arg,SCRIPT)){
				if(isset($params)){
					$params = (!is_array($params))? (array)$params : $params;
				} else {
					$params = array();
				}
				$this->params = (object)array_merge($params,(array)$this->params);  // Merges this method's params with the URL and form params, where URL and form params are master/overwrite duplicates.
				$this->obj = $obj;
				$this->scripts = SCRIPTS.$arg.'.'.EXT;
				return TRUE;
			}
		} catch(\Exception $e) {
			print('Invalid Script (script not specified, not enabled, and not default to this template: '.$e->getMessage()."\n");
			return FALSE;
		}	
	}

	/**
	/*	setPage
	/*	If a module is called by the view file, we set it to this object. // REFACTOR 0
	**/

	/**
	/*	setContent
	/*	If content is called by the view file, we set it to this object. // REFACTOR 0
	**/

	/**
	/*	getScripts
	/*	Loads specified script from $arg.
	**/
	public function getScripts($arg,$obj=NULL,$params=NULL){
		try{
			self::setScripts($arg,$obj,$params);
			require_once($this->scripts);
			return TRUE;
		} catch(\Exception $e) {
			print('Invalid Script (script not specified, not enabled, and not default to this template: '.$e->getMessage()."\n");
			return FALSE;
		}
	}

	/**
	/*	getModule
	/*	Loads specified module from $arg. If no arg is present, it will load the module specified by the template's name and factory node type.
	**/
	public function getModule($arg=NULL){
		try{
			self::setModule($arg);
			return new $this->module;
		} catch(\Exception $e) {
			print('Invalid Module (module not specified, not enabled, and not default to this template: '.$e->getMessage()."\n");
			return FALSE;
		}
	}
}

?>
