<?php
namespace Core\Models;

use \Core\Controllers\Factory as Factory;

Class Content implements Node {
	
	/** 
	/*	getAll Method
	/*  This method takes an input of settings-specified URLs.  No template or URL validation is run, it's just static data.
	/*  Content Types are not automatically loaded by traversing the folder, they're registered in the settings.
	*/
	public function getAll(){
		return Factory::nodeArray(CONTENT);
	}
	
	public function getURL($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	public function getID($arg,$array){
		return array_search($arg,$array);
	}
	
	public function getTitle($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	public function getContent($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	/** 
	/*  setHierarchy method determines a content type URL's hierarchy (single, listing, archive).
	**/
	public function setHierarchy($url){
			if(array_key_exists(1,$url)&&strtolower($url[1]==ARCHIVE)){
				return ARCHIVE;
			}elseif(array_key_exists(1,$url)&&strlen($url[1])>=1){
				return SINGLE;
			}else{
				return LISTING;
			}
	}
	
	
	
	/** Abstract Methods
	/*  Necessary for building the sub-nodes
	*/
//	abstract public function setName();
//	abstract public function getName();
//	abstract public function alerts();
		//If a content type, module, or page name conflicts, we need to alert the administrator in the browser.
		//Content Type trumps Module (deactivates module).  Both trump page (deactivates page)

}

?>