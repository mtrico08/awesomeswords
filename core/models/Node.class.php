<?php
namespace Core\Models;

/** 
/*	Node Abstract Class.
/*  This sets up the framework for building and validating each node structure
/*  This is implemented by all node models.
*/

interface Node {
	
	public function getAll();
	
	public function getURL($arg,$array);
	
	public function getID($arg,$array);
	
	public function getTitle($arg,$array);
	
	public function getContent($arg,$array);
}

?>