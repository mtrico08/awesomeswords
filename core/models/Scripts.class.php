<?php
namespace Core\Models;

use \Core\Controllers\Factory as Factory;

class Scripts implements Node {
	
	private $all;
	
	/** 
	/*	getAll Method
	*/	
	public function getAll(){
		// REFACTOR 0: If Module, Page, and Content have the same methods, move to a parent
		// We used to pre-register modules.  Then we would auto-load all but the deactivated ones with // array_diff(scandir(MODULE),array_merge(Factory::nodeArray(MOD),array('..','.'))); 
		// Now, we explicitly require them to be enabled, instead, so that code isn't needed.
		$this->all = Factory::nodeArray(SCRIPT);
		return $this->all;
	}
	
	public function getURL($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	/**
	/* getID typically returns the key of a page within the sitemap but in the case of scripts, we just return the URL back
	**/
	public function getID($arg,$array=NULL){
		$array = (isset($array)&&!empty($array))? $array : $this->all;
		// if(NULL != ($key = array_search($arg,$array))){ || REFACTOR 0: REMOVE
		if(in_array($arg,$array)){
			return $arg;
		} else {
			return FALSE;
		}
	}
	
	public function getTitle($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	public function getContent($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	/** 
	/*  setHierarchy method determines a page's hierarchy (parent/child): REFACTOR 0: This isn't done
	**/
	public function setHierarchy($url){
		return $url;
	}

	/** Abstract Methods
	/*  Necessary for building the subnodes // REFACTOR 0
	/*
	*/
//	abstract public function setName();
//	abstract public function getName();
//	abstract public function alerts();
		//If a content type, module, or page name conflicts, we need to alert the administrator in the browser.
		//Content Type trumps Module (deactivates module).  Both trump page (deactivates page)

}

?>