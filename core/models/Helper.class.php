<?php
namespace Core\Models;

/** Class Helper
/*  This class contains a variety of global algorithms that may be used by models and controllers alike.  It's basically a controller.  REFACTOR 0: Should this be in controllers?
/*  These methods are used throughout the framework and for that reason, are static
/*  As these methods are all static, there's an inherent flaw in dependency.  Cross dependency within helper is a required benefit
*/

use \Core\Controllers\Factory as Factory;

Class Helper {

	public $cat_cache;
	public $fxn;
	public $array_temp;
	public $src;
	public $src_object;
	public $src_namespace;

	public function __construct(){
		$this->array_temp = array();
	}

	/**
	/*	urlParser method
	/*  Creates Array of a URL argument and drops the trailing slash
	/*	If no parameter is provided, it will retrieve this, itself
	/*	This is a critical function for the framework.
	**/
	public static function urlParser($url=null,$domain=null,$title=null,$tagline=null){
		/** URL handler **/
		$url = (is_null($url))? $_SERVER['REQUEST_URI'] : $url;
		$url = Helper::clean(explode('/',ltrim($url,'/')));  // Drop the leading slash
		$par = substr(end($url),strpos(end($url),'?')+1);
		$par = explode('&',$par);
		$params = array();
		foreach($par as $param){
				$val = substr($param,strpos($param,'=')+1);
				$key = strtok($param,'=');
				$params[$key] = $val;
		}
		$count = abs(count($url)-1);
		$url[$count] = strtok($url[$count],'?'); // Drop the params from the final part.
		reset($url);
		/** Domain handler **/
		$domain = (is_null($domain))? $_SERVER['HTTP_HOST'] : $domain;
		/** Title handler **/
		if(is_null($title)):
			if (substr($domain, 0, 4) == 'www.') {
				$title = ucfirst(substr($domain, 4));
			} else {
				$title = ucfirst($domain);
			}
		endif;
		$page = array_slice($url, -1);
		$page = array_pop($page);
		$page = (!empty($page)&&(strlen($page)>=2))? $page : $tagline;
		$title = $page.' | ' .$title;
		/** End handlers, begin object build **/
		return (object) array('domain'=>$domain,'url'=>$url,'title'=>$title,'tag'=>$tagline,'params'=>$params);
	}

	/**
	/*	Namer Method
	/*	Builds Namespaces within a class, from current or specified directory.
	/*	REFACTOR 0: Use this in the build method? Or remove.
	**/
	public static function namer($dir){
		$dir = (isset($dir))? $dir : __DIR__ ;
		$current = str_replace("/","\\",$dir);
		$cut = strlen(ROOT);
		$ns = substr($current,$cut);
		return $ns;
	}

	/**
	/*	File Converter Method
	/*  Explodes filenames to an array to read and return the prefix to an array
	/*  May also take a second argument to return the file base name
	/*  Used primarily for modules but may also be used for content subtypes
	/*	This is a critical function for the framework.
	**/
	public static function fileconverter($arg,$num=0){
		$explode = explode('.',$arg);
		$explode = (isset($explode[$num])) ? $explode[$num] : $explode[0];
		$result = self::clean(strtolower($explode));
		return $result;
	}

	/**
	/*	Traverse Dir Filename
	/*	Used to return full filenames from directories
	/*	If a specific extension is passed, it will validate objects for that extension, too.
	/*	This is a critical function for the framework
	**/
	public static function traverseDirFilename($directory,$ext=NULL){
		$objects = scandir($directory,0);
		$results = array();

		foreach($objects as $object){
			if((strlen(self::fileconverter($object))>0)):
				if(!is_null($ext)){
					if(strpos($object,'.'.$ext)){
						$results[] = $object;
					}
				} else {
					$results[] = $object;
				}
			endif;
		}
		return $results;
	}

	/**
	/*	sourceValidate validates API sources.
	/*	accepts extended validation against existing nodes with "$ev=1"
	**/
	public function sourceValidate($src,$ev=0){
		try {
			if($ev==1){
				$all_nodes = array_unique(self::siteMap());
				if(in_array($src,$all_nodes)){
					return true;
				}
			} else {
				$this->src = $src;
				$this->src_namespace = '\\Modules\\'.$this->src.'\\Models\\'.$this->src;
				$this->src_object = new $this->src_namespace;
				return $this->src_object;
			}
		} catch (Exception $e) {
			print 'Source Required';
		}
	}

	/**
	/*	functionBuild creates function names from inputs for referencing the correct setters
	/*	This is a critical function for scalability.
	**/
	protected function functionBuild($type,$src=NULL,$prefix=NULL){
		if(!isset($src)){
			$src = $this->src;
			$object = $this->src_object;
		} else {
			$this->src_object = $object = self::sourceValidate($src);
		}
		$prefix = isset($prefix)? $prefix : 'set';
		$this->fxn = $prefix.ucfirst($type);
		if(!method_exists($object,$this->fxn)){
			throw new \Exception('Class '.$src.' Method '.$this->fxn.' Method missing.');
		}
		return $this->fxn;
	}

	/**
	/*	cleanVars sanitizes variabls, builds cache file, and creates vars for getters and setters
	/*	Always Clean Vars before passing them, as only numeric vars may actually retrieve data from type.
	**/
	protected function cleanVars(&$type,&$var,$src=NULL){
		if(!is_null($src)){
			self::sourceValidate($src);
		}
		// For specific ID's.
		if(!is_numeric($var)&&(strtolower($var)=='all')) {
			$str = 'All';
			$var = (strcasecmp($src, 'Ebay') == 0)?	'-1' : $str;	//REFACTOR 0: This should be generic, so we'll need a better way of passing this value as "all" before it reaches here.
		} else {
			$str = $var;
		}
		$type = ucfirst($type);
		$this->cat_cache = ROOT.'/cache/'.strtolower($src).'_'.strtolower($type).'_'.strtolower($str).'.json';
	}

	/**
	/*	Clean Method
	/*  Removes special characters and spaces to sanitize URLs, filenames, arrays and strings
	/*  Converts to lowercase for comparison operators
	**/
	public static function clean($arg){
		$result = array();
		$search = array('%20',' ');
		if(!is_array($arg)){
			//Replaces special characters with nothing
			$result = preg_replace('/[^a-zA-Z0-9\']/','',$result);
			//Replaces spaces with underscores
			$result = str_replace($search,"_",strtolower($arg));
		} else {
			foreach($arg as $key=>$value){
				$result[self::clean($key)] = self::clean($value);
			}
		}
		return $result;
	}

	/**
	/*	getModPart
	/*	Checks for a module-based partial and if it exists, returns that, otherwise it returns false.
	/*	This allows developers to overwrite a module's default partial within their own theme, simply by labeling it module.[modulename].[partialname].php
	**/
	public function getModPart($module,$part){
		if(file_exists(PARTIAL.MOD.'.'.$module.'.'.$part.'.'.EXT)){
			return PARTIAL.MOD.'.'.$module.'.'.$part.'.'.EXT;
		} else {
			return false;
		}
	}

	/**
	 *	setCache
	 *	builds the cache for any given set function of a Module and writes to it.  Also allows data to write, explicitly.
	**/
	public function setCache($type='categories',$var='all',$src=NULL,$obj=NULL){
		// Validates that SRC exists and is a valid node for the src::fxn call
		try {
			if(NULL !== $obj){
				self::sourceValidate($src,1);
				self::cleanVars($type,$var,$src);
				self::functionBuild($type);
				$fxn = $this->fxn;
				if(!isset($this->src_namespace)){
					$namespace = '\\Modules\\'.$src.'\\Models\\'.$src;
					$module = new $namespace;
				} else {
					$module = new $this->src_namespace;
				}
				$obj = $module->$fxn($var);  // Refactor. getData allows for 2 vars.  Not sure if necessary.
			}
			file_put_contents($this->cat_cache,json_encode($obj));
			return true;
		} catch (Exception $e) {
			print 'Failed to build cache. File '.$this->cat_cache.' or function '.$src.' '.$fxn.' invalid';
		}
	}

	/**
	 *	getData function reads data from API or cache file
	 *	It validates the model's method, sanitizes variables, then builds the function name
	 *	and calls it if the cache file containing the same params does not exist.
	 *  It also builds cache files in the background if it does not exist.
	 *	Can't use ternary operator since can't declare self::$this->fxn
	 *	Accepts "$write" param to allow you to choose to write data to a cache file or postpone reading and writing if running getData loops.
	**/
	public function getData($type='categories',$var='all',$src=NULL,$var2 = NULL,$position=1,$count=100,$write=TRUE){
		self::sourceValidate($src,1);
		self::cleanVars($type,$var,$src);
		self::functionBuild($type);
		// This ignores alpha vars, per cleanVars, above
		if(file_exists($this->cat_cache) && (filemtime($this->cat_cache) > (time() - 60*60*24*30))){
			$result = json_decode(file_get_contents($this->cat_cache));
		} else {
			$fxn = $this->fxn;
			if(!isset($this->src_namespace)){
				$namespace = '\\Modules\\'.$src.'\\Models\\'.$src;
				$module = new $namespace;
			} else {
				$module = new $this->src_namespace;
			}
			// REFACTOR 0: This next line is for pagination... Should this whole function move?
			// P.s. we use SetPagination in the controller, too...
			$module->setPagination($position,$count);
			$result = $module->$fxn($var,$var2);
			// We don't need to continue processing data, the cache method will do that for us.
			if(TRUE===$write){
				// REFACTOR 0:  setCache doesn't permit for 2 vars, yet.
				shell_exec(sprintf('%s > /dev/null 2>&1 &', self::setCache($type,$var,$src,$result)));
			}
		}
		return $result;
	}

	/**
	/*	recursiveIterateKeyMatch returns all values of 1st arg (array or object)
	/*	which matches 2nd arg (key).  May also be used without a keymatch to return
	/*	all results in a flat array
	/*	REFACTOR 0: Only used by Amazon module right now
	**/
	public function recursiveIterateKeyMatch($obj_array,$keymatch=NULL){
		foreach($obj_array as $key=>$item){
			if(isset($keymatch)&&(strtolower($key)==strtolower($keymatch))){
				$this->array_temp[] = $item;
			} elseif(count($item)>0){
				self::recursiveIterateKeyMatch($item,$keymatch);
			} elseif(!isset($keymatch)&&(count($item)==0)){
				$this->array_temp[] = $item;
			}
		}
		return $this->array_temp;
	}

	/**
	/*	caseInsensitivePropertyAccess helps with inconsistent property keys such as ItemID vs itemId
	/*	Accepts arguments of an object and its property and checks the object for properties matching a variety of cases.
	**/
	public function caseInsensitivePropertyAccess($object,$property){
			foreach($object as $key=>$prop){
				if(strtolower($key)==(strtolower($property))){
					return $object->$key;
				}
			}
			return false;
	}

	/**
	/*	getCompare returns a ranking (max 100) of similarity between 2 strings.
	**/
	public function getCompare($var,$eval,$rank=NULL){
		$var = str_replace("+", " ", $var); // strings with spaces areconverted to +, so we'll remove +, here.
		$rank = (isset($rank))? $rank : 100; // rank starts at 100.
		if(is_string($eval)){
			if ($var==$eval){
				return $rank;
			} else {
				$cnt=count($exploded=explode(' ',$eval));
				if ((strpos(Helper::clean($eval),Helper::clean($var))!== false)||((isset($eval)&&$eval!='')&&strpos(Helper::clean($var),Helper::clean($eval))!== false)){
					$var_length = strlen($var);
					$rank = (substr($eval,0,$var_length)==$var)? $rank - (1*$cnt) : $rank - (2*$cnt);
					return $rank;
				} else {
					$rank1 = ($rank - levenshtein(Helper::clean($var),Helper::clean($eval)));
					// Similarity order of variables is important.  Time Complexity = O(N**3) where N is longest string
					similar_text($eval,$var,$similarity);
					$rank2 = $rank - $similarity;
					if(($rank = max($rank1,$rank2))>75){
						return $rank;
					} else {
						if ($cnt>1) {
							// If all our tests fail, we break it down and re-run evaluations against each word in a phrase
							foreach($exploded as $word){
								return self::getCompare($var,$word,$rank);
							}
						} else {
							return $rank;
						}
					}
				}
			}
		} else {
			foreach($eval as $word){
				return self::getCompare($var,$word);
			}
		}
	}

	/**
	/*	getMenu displays menu based on the sitemap from settings
	/*	Accepts array argument for manually built sitemap
	**/
	public function getMenu($arg = NULL,$classes=null){
		$list = (isset($arg))? $arg : self::siteMap();
		$class = $types = array('ul'=>'','li'=>'','a'=>'');
		// REFACTOR 0: Maybe we should be pulling the types of classes that the user wants, from the actual settings file, instead of pulling it from core.  That way they can use any names they want; it doesn't have to be ul/li/a.  It could instead, follow hierarchy or something else?
		if(!empty($classes) && is_array($classes)){
		  foreach($types as $type=>$value){
			  if(null !==($name = $classes[$type.'class'])){
				$class[$type] = $name;
			  }
		  }
		}
		$menu = '<ul class="'.$class['ul'].'">';
		foreach($list as $key=>$item){
			$title = (!is_numeric($key))? $key : $item;
			if($title=='home'){ $item = ''; }
			if($title==''){ continue; }
			$menu .= '<li class="'.$class['li'].'"><a class="'.$class['a'].'" href="/'.$item.'">'.$title.'</a></li>';
		}
		$menu .= '</ul>';
		echo($menu);
		return TRUE;
	}

	/**
	/*	siteMap
	/*	creates a single level array of URL's where key becomes page ID.  value becomes page name.
	/*  This is a static function, unlike the other methods in this file, so it cannot use its object properties.
	**/
	public static function siteMap(){
		try{
			$all_nodes = Factory::nodeArray();
			return array_merge($all_nodes[PAGE],$all_nodes[CONTENT],$all_nodes[MOD]);
		} catch(\Exception $e) {
			print('Caught exception: '.$e->getMessage()."\n");
		}
	}

	/**
	/*	validateURL Method
	/*	Accepts: url to validate against node (array of nodes, node type string, or all content nodes)
	/*  Checks if the URL input from is in the array provided
	/*	Returns Boolean (TRUE:FALSE)
	**/
	public static function validateURL($url,$node=NULL){
		if(is_null($node)||!is_array($node)){
			$node = (!is_null($node))? Factory::nodeArray()[$node] : Factory::nodeArray()[CONTENT];
		}
		if(is_array($url)){
			$url = implode('/',$url);
			self::validateURL($url,$node);
		}
		return in_array(self::clean($url),$node);
	}

	/**
	 *	REFACTOR 0:  If this is only used once elsehwere, move out of Helper class.
	 *	validateView Method
	 *	Accepts: $url (url to validate), $type (node type), and optional $dir (template directory to check)
	 *  Checks if the URL input matches a view file and returns closest possible template.
	 *  Used in validation, after the validateURL method.
	 *  Content types are checked for the archive, single, and listing, as well as filename without prefix.
	 *		template => array(listing/archive => array( // nested pages w/ AI keys ));
	**/
	public static function validateView($url,$type,$dir=TEMPLATE){
		$bases = array();
		$suffixes = array();
		$array = self::traverseDirFilename($dir);
		$nodeArray = Factory::nodeArray();
		foreach($array as $file){
			$prefix = self::fileconverter($file,0);
			$bases[] = $prefix;
			if(($suffix=self::fileconverter($file,1))!='php'){
				$suffixes[$prefix][] = $suffix;
			}
		}
		$url = self::clean($url);
		if(is_array($url)){
			if($type==PAGE||$type==SCRIPT){
				if((count($url)==1)&&strlen($url[0])==0){ $url[0]='home'; }
				$template = ($temp=array_search(end($url),$bases))? $bases[$temp] : DEF;
			} elseif($type==CONTENT||$type==MOD){
				$template = ($temp=array_search($url[0],$bases))? $bases[$temp] : DEF;
				if($type==CONTENT&&count(array_filter($url))>1){
						/**
						 *	Content types may have several bases and subpages because of (archive, listing, single).
						 *	Hence, we use "base" as the key (ie. settings page).
						 *	Listing and Archive URL's will always indicate "archive" or "listing" as 2nd.
						 *	Filenames will, too.  If no suffix, it will default to "listing".
						 **/
						if(in_array($url[1],$suffixes[$template])){
							$template = (($url[1]==ARCHIVE||$url[1]==LISTING)&&(empty(end($url))))? $template.'.'.$url[1] : $template.'.'.SINGLE;
						} else {
							$template = (($url[1]==LISTING)&&(empty(end($url))))? $template : $template.'.'.SINGLE;
						}
				}
			}
		} else {
			$template = ($temp=array_search($url,$array))? $array[$temp] : DEF;
		}
		return $template;
	}
	/**
	 * requireToVar
	 * Accepts a file argument and requires that file, leveraging output buffering.
	 * Accepts 2nd argument of a passed object to be used in the partial.
	 * @param  file $file provided file
	 * @return sqlite_escape_string
	**/
	public function requireToVar($file,$obj){
		ob_start();
		require($file);
		return ob_get_clean();
	}

}

?>
