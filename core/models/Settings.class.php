<?php
namespace Core\Models;

	/** 
	/*	Class Settings
	/*  Retrieves the administrator settings.  Some methods may resemble controllers/actions but in reality, this class is directly interfacing with our "data".
	/*  These settings will be used by all node models.  Models will check if true before using these settings.
	/*  With this class, we could easily change settings to XML, or a large array, or contained in the DB and simply update this class without interrupting the rest of the framework. (<-- REFACTOR 1)
	/*  Getters and Setters are largely unnecessary versus accessing the public properties from the settings directly but adds a level of validation and reduces direct access to a less secure directory.
	/*  Security actions should be built into this class, to review potential hazards from "settings" as well. (<-- REFACTOR 1)
	/*  Helper class methods can be used to sanitize the user's inputs into this class as well.  That should happen through a controller, though.
	*/

Class Settings {

	public $config;
	public $array_results;
	public $pr;
	public $depth;
	public $level;
	
	public function __construct(\Customize\Config\Settings $config){
		$this->array_results = array();
		$this->depth = 0;
		$this->level = array();
		$this->pr = '';
		return $this->config = $config;
	}
	
	public function getTitle(){
		return !empty($this->config->title)? $this->config->title : NULL;
	}

	public function getTagline(){
		return !empty($this->config->tagline)? $this->config->tagline : NULL;
	}

	public function getPages(){
		return !empty($this->config->pages)? self::getUrlKey($this->config->pages) : FALSE;
	}

	public function getContent(){
		return !empty($this->config->content_type)? self::getUrlKey($this->config->content_type) : FALSE;
	}

	public function getScripts(){
		return !empty($this->config->scripts)? Helper::clean($this->config->scripts) : FALSE;
	}
	
	public function getModules(){
		return !empty($this->config->activate_module)? Helper::clean($this->config->activate_module) : FALSE;
	}
	
	public function getCss(){
		return !empty($this->config->css_order)? $this->config->css_order : FALSE;
	}
	
	public function getJs(){
		return !empty($this->config->js_order)? $this->config->js_order : FALSE;
	}
	
	/** clearEmpty is just a recursive function used by getUrlKey, to pop off closed arrays
	**/
	public function clearEmpty($arg){
		if(end($arg)==1){
			array_pop($arg);
			self::clearEmpty($arg);
		} 
		return $arg;
	}
	
	/** 
	/*	getUrlKey gets the name and URL of a corresponding array, for a flat array
	/*	Depth counts how deep we've gone in the array
	/*	Level Stores the parent count, so we can see if we're done with the last loop
	**/
	public function getUrlKey($var){
		if(!empty($var)&&(is_array($var))){
			$i = 0;
			$count = count($var);
			$this->level[] = $count;
			foreach($var as $key=>$item){
				$title = (!is_array($item))? $item : $key;
				$this->depth = count($this->level);
				$key = Helper::clean($key);
				$i++;
				if (false !== in_array(Helper::clean($item),$this->array_results)){ continue; } else {
					if(!is_array($item)){
						$item = Helper::clean($item);
						if(($item!=FALSE)&&(strlen($item)>0)){
							// Substr drops the leading "/". We'll add that back later.
							$key = (is_numeric($key))? '' : '/'.$key ;
							// REFACTOR 0: ^ This is a problem if two sibling pages have the same title.
							$url = $this->pr.$key.'/'.$item;
							$this->array_results[$title] = (substr($url,0,1) == '/') ? substr($url,1) : $url;
						}
						if($count==$i) {
							if($this->depth>0){
								array_pop($this->level);
								$this->level = self::clearEmpty($this->level);
								$this->depth = count($this->level);
							}
							$exp = explode('/',$this->pr);
							$exp = array_slice($exp, 0, $this->depth);
							$this->pr = implode('/',$exp);
						}
					} else {
						$this->array_results[$title] = ($this->pr=='')? $key.'/' : substr($this->pr,1).'/'.$key.'/';
						// If we've reached the end of the array, set the parent count to 1
						if($count==$i) {
							array_pop($this->level);
							$this->level[] = 1;
						}
						$this->pr = $this->pr.'/'.$key;
						self::getUrlKey($item);
					}
				}			
			}
			return $this->array_results;
		} else {
			return FALSE;
		}
	}
}

?>