<?php
namespace Core\Models;

use \Core\Controllers\Factory as Factory;

class Page implements Node {
	
	/** 
	/*	getAll Method
	/*  This method takes an input of settings-specified URLs.  No template or URL validation is run, it's just static data.
	/*  Content Types are not automatically loaded by traversing the folder, they're registered in the settings.
	*/
	public function getAll(){
		return Factory::nodeArray(PAGE);
	}
	
	public function getURL($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	public function getID($arg,$array){
		return array_search($arg,$array);
	}
	
	public function getTitle($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	public function getContent($arg,$array){
		return (isset($array[$arg])) ? $array[$arg] : FALSE;
	}
	
	/** 
	/*  setHierarchy method determines a page's hierarchy (parent/child): REFACTOR 0: This isn't done
	**/
	public function setHierarchy($url){
		return $url;
	}

}

?>