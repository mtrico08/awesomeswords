<?php
namespace Core\Models;

Class Constants
{
	public function __construct(){
	/* constants for directory naming conventions */
			define('DOMAIN',$_SERVER['HTTP_HOST'].'/');
			define('ROOT',DOMAIN.'/../');
			//define('ROOT',$_SERVER['DOCUMENT_ROOT'].'/');	// When migrating, you may need to enable this.
			define('HTTPD',(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://");
			define('MODEL','models/');
			define('CONTROLLER','controllers/');
			define('CORE',ROOT.'core/');
			define('CUSTOM',ROOT.'customize/');
			define('CONFIG',CUSTOM.'config/');
			define('SETTINGS',CONFIG.'settings.class.php');
			define('MODULE',ROOT.'modules/');
			define('SCRIPTS',ROOT.'scripts/');
			
	/* constants for logic and file naming conventions */
			define('ARCHIVE','archive');
			define('LISTING','listing');
			define('SINGLE','single');
			define('CONTENT','content');
			define('SCRIPT','scripts');
			define('MOD','module');
			define('PAGE','page');
			define('HOME','home'); // home template filename
			define('DEF','default'); // default template filename
			define('NONE','404'); // 404 template filename
			define('EXT','php'); // view file extension (yes, this is a bit unnecessary)
			define('HEAD','header'); // header partial filename
			define('FOOTER','footer'); // header partial filename
			define('META','meta'); // header extension meta filename
			define('NAVIGATION','menu'); // header extension nav filename
	}
	public function setTheme($theme){
			/** Important Note										*/
			/*	The "THEME" constant is set up by the settings file */
			define('THEME',$theme);
			define('VIEW',ROOT.'views/'.THEME.'/');
			define('PARTIAL',VIEW.'partials/');
			define('TEMPLATE',VIEW.'templates/');
			// REFACTOR 0 - include module based partials
			define('CSS',VIEW.'pub/css/');
			define('IMG',VIEW.'pub/img/');
			define('JS',VIEW.'pub/js/');
			return TRUE;
	}
}

?>