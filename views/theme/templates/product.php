<?php
$num = 50;					// num results per page
$var = 'game of thrones';	// search term
$module = $this->getModule();	// instantiates the module
?>
<?php $this->partial('filters'); ?>
<!-- START OF TEMPLATE -->
<div class="container-fluid main">
  <div class="sidebar-margin">
    <div class="products masonry-grid">
      <?php
	  $module->showItemsByCategory($var,$num);
      //$this->getScripts('product_loader',$module,['var'=>$var,'num'=>$num]);
      ?>
    </div>
    <div class="text-center mt-1 mb-3">
      <a href="#" class="btn btn-primary" data-action="loadMore" data-type="product" data-num="<?php echo $num; ?>" data-var="<?php echo $var; ?>" data-last="<?php echo $num; ?>" data-page="1" >Load More</a>
    </div>
  </div>
</div>
<!-- END OF TEMPLATE -->