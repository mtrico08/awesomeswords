<div class="product masonry-grid-item" data-price="<?php echo str_replace('$','',$this->obj->price); ?>" data-cat="<?php echo (isset($this->obj->cat))?$this->obj->cat:'';?>">
  <div class="product-inner">
    <a href="<?php echo (isset($this->obj->url))?$this->obj->url:'#';?>" title="<?php echo (isset($this->obj->name))?$this->obj->name:'';?>">
      <img class="product-img" src="<?php echo (isset($this->obj->img))?$this->obj->img:'blank.jpg';?>" alt="<?php echo (isset($this->obj->name))?$this->obj->name:'';?>">
    </a>
    <div class="product-desc">
      <h1><?php echo (isset($this->obj->name))?$this->obj->name:'';?></h1>
      <div class="product-cat">
        <?php echo (isset($this->obj->cat))?$this->obj->cat:'';?>
      </div>
      <div class="row mt-sm-2 mb-sm-2">
        <div class="col-md-4 small">
          Ranking:
        </div>
        <div class="col-md-8">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $this->obj->rank ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $this->obj->rank ?>%">
              <?php echo $this->obj->rank ?> out of 100
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="product-price">
            <?php
            $price_array = explode('.',$this->obj->price);
            ?>
            <?php echo current($price_array); ?>
            <sup>
              <?php echo end($price_array); ?>
            </sup>
          </div>
        </div>
        <div class="col-md-6">
          <a href="<?php echo $this->obj->url ?>" class="float-right product-buy btn btn-primary">Buy</a>
        </div>
      </div>
    </div>
  </div>
</div>
