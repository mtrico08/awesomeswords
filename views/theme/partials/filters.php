<div class="product-filter text-center">
  <div class="product-filter-container bg-inverse text-left">
    <div class="wrapper container-fluid">
      <div class="">
        <div class="priceslider-label product-filter-label">
          Price:
        </div>
        <div class="priceslider-container">
          <div id="priceslider" >
          </div>
        </div>
      </div>
      <hr>
      <div class="">
        <div class="product-filter-label">
          Categories:
        </div>
        <div class="categoryselector">
        </div>
      </div>
    </div>
  </div>
	<div class="product-filter-toggler bg-inverse hasAction mobile-only" data-action="toggleDiv" data-target="product-filter-container">
	  Filters <i class="fa fa-chevron-cirdle-down"></i>
	</div>
</div>
