<!-- START OF TEMPLATE -->
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
	<div class="wrapper">
	  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>
	  <a class="navbar-brand" href="#">Awesomeswords</a>
	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<?php
		use \Core\Models\Helper as Helper;
		Helper::getMenu(null,['ulclass'=>'navbar-nav mr-auto','liclass'=>'nav-item','aclass'=>'nav-link']);
		?>
		<form class="form-inline my-2 my-lg-0">
		  <input class="form-control mr-sm-2" type="text" placeholder="Search">
		  <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
		</form>
	  </div>
	</div>
</nav>
<!-- END OF TEMPLATE -->
