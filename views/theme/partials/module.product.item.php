<div class="product masonry-grid-item" data-count="<?php echo $obj->i; ?>" data-price="<?php echo str_replace('$','',$obj->price); ?>" data-source="<?php echo (isset($obj->source))?$obj->source:'';?>" data-cat="<?php echo (isset($obj->cat))?$obj->cat:'';?>">
  <div class="product-inner">
    <a target="_blank" href="<?php echo (isset($obj->url))?$obj->url:'#';?>" title="<?php echo (isset($obj->name))?$obj->name:'';?>">
      <img class="product-img" src="<?php echo (isset($obj->img))?$obj->img:'blank.jpg';?>" alt="<?php echo (isset($obj->name))?$obj->name:'';?>">
    </a>
    <div class="product-desc">
      <h1><?php echo (isset($obj->name))?$obj->name:'';?></h1>
      <div class="product-cat">
        <?php echo (isset($obj->cat))?$obj->cat:'';?>
      </div>
      <div class="row mt-sm-2 mb-sm-2">
        <div class="col-md-4 small">
          Ranking:
        </div>
        <div class="col-md-8">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $obj->rank ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $obj->rank ?>%">
              <?php echo $obj->rank ?> out of 100
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="product-price">
            <?php
              $price_array = explode('.',$obj->price);
              $price_first = current($price_array);
              if (strpos($price_first,'$') === false) {
                $price_first = '$'.$price_first;
              }
            ?>
            <?php echo $price_first; ?>
            <sup>
              <?php echo end($price_array); ?>
            </sup>
          </div>
        </div>
        <div class="col-md-6">
          <a target="_blank" href="<?php echo $obj->url ?>" class="float-right product-buy btn btn-primary" title="<?php echo (isset($obj->name))?$obj->name:'';?>">Buy</a>
        </div>
      </div>
    </div>
  </div>
</div>