/************************************************/
/* Add Your Tracking Scripts (ie. google) below */
/*************************************************/
jQuery(document).ready(function($){
  "use strict";
  console.log('jQuery Ready');
  var Site = function(){
    var b,products,f,formData,pmin,pmax,masonry,filterCategories,filterPrices,action;
    var bLoading = function() {
      if(b.hasClass('loading')){
        b.removeClass('loading');
      } else {
        b.addClass('loading');
      }
    };
    var ajaxCallback = function(u, c, t) {

      t = typeof t !== 'undefined' ? t : 'POST';
      bLoading();
      if (formData) {
        $.ajax({
          type: t,
          url: u,
          data: formData,
          // dataType: 'json'
        }).done(function (d, status, xhr) {
        // }).done(function(d) {
          bLoading();
          c(d);
        }).fail(function(jqXHR, textStatus) {
          console.log("Request failed: " + textStatus);
          bLoading();
        });
      }
    };
    var initButton = function(){
      $(document).on('click','.btn, .hasAction',function(e){
        b = $(this);
        if(b.data('action') && !b.hasClass('loading')){
          e.preventDefault();
          if(b.closest('form').length){
            f = b.closest('form');
            formData = f.serialize();
          }
          actions();
        }
      });
    };
    var actions = function(action){
      action = typeof action !== 'undefined' ? action : b.data('action');
      switch (action) {
        case 'loadMore':
          formData = {
            'var':b.data('var'),
            'num':b.data('num'),
            'last':b.data('last'),
            'page':b.data('page'),
          };
          ajaxCallback('/product_loader', function(d) {
            d = $.parseJSON(d);
            if (d.status == true){
              if($('.products').length){
                $('.products').append(d.view);
                setTimeout(function(){ initProducts(); updateProducts() }, 500);
                // setTimeout(function(){ updateProducts(); }, 1000);
              }
              b.data('page',d.page);
            } else {
              b.html(d.message);
            }
          });
          break;
        case 'toggleDiv':
          var target = b.data('target');
          var targetObj = $('.'+target);
          if(targetObj.length){
            if(b.hasClass('product-filter-toggler')){
              $('.sidebar-margin').toggleClass('sidebar-margin-collapsed')
            }
            targetObj.slideToggle().toggleClass('slideToggled');
          }
          break;
        case 'categoryFilter':
          filterCategories = [];
          b.toggleClass('category-selected');
          $.each($('li[data-action="categoryFilter"].category-selected'),function(i,v){
            filterCategories.push($(v).data('cat'));
          });
          updateProducts();
          break;
        default:
          console.log('Button has no action defined');
          break;
      }
    };
    var initMasonry = function(){
      if ($('.masonry-grid').length && $.fn.masonry) {
        masonry = $(".masonry-grid");
        masonry.imagesLoaded(function () {
          masonry.masonry({
            gutter: 20,
            fitWidth: true,
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.masonry-grid-item',
            // use element for option
            // columnWidth: '.grid-sizer',
            // percentPosition: true
          });
        });
      }
      checkBottom(function(){
        // action = 'loadMore';
        b = $('a[data-action="loadMore"]');
        actions();
      })
    };
    var checkBottom = function(c){
      $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
          c();
        }
      });
    };
    var initProducts = function(callback){
      if($('.products').length){
        var prices = [];
        var allCategories = [];
        $.each($('.product'),function(i,v){
          prices.push(parseFloat($(v).data('price')));
          if(!allCategories.includes($(v).data('cat'))){
            allCategories.push($(v).data('cat'));
          }
        });
        prices = prices.sort(sortNumber);
        pmin = prices[0];
        pmax = prices[prices.length-1];
        initPriceSlider();
        initCategorySelector(allCategories);
        filterCategories = allCategories;
        // callback();
      }
    };
    var sortNumber = function(a,b){
      return a - b;
    };
    var initCategorySelector = function(categories){
      $.each(categories,function(i,v){
        $('.categoryselector').append('<li data-action="categoryFilter" class="category-selected hasAction" data-cat="'+v+'">'+v+'</li>');
      });
    };
    var initPriceSlider = function(){
      if($('#priceslider').length && pmin && pmax){
        var priceslider = document.getElementById('priceslider');
        if($('#priceslider').hasClass('noUi-target')){
          priceslider.noUiSlider.updateOptions({
            start: [pmin, pmax],
            range: {
              'min': pmin,
              'max': pmax
            }
          });
        } else {
          noUiSlider.create(priceslider, {
            start: [pmin, pmax],
            connect: true,
            tooltips: true,
            range: {
              'min': pmin,
              'max': pmax
            }
          });
        }
        filterPrices = [pmin,pmax];
        priceslider.noUiSlider.on('change',function(values, handle, unencoded, tap, positions){
          filterPrices = values;
          updateProducts();
        });
      }
    };
    var updateProducts = function(){
      if($('.products').length){
        $.each($('.product'),function(i,v){
          if(
            parseFloat(filterPrices[0]) <= parseFloat($(v).data('price')) && parseFloat($(v).data('price')) <= parseFloat(filterPrices[1]) && filterCategories.includes($(v).data('cat'))
          ){
            $(v).show().addClass('masonry-grid-item');
          } else {
            $(v).hide().removeClass('masonry-grid-item');
          }
        });
        masonry.masonry('reloadItems');
        masonry.masonry('layout');
      }
    };
    var init = function(){
      initProducts();
      initMasonry();
      initButton();
      // checkBottom();
      // initPriceSlider();
      console.log('Site Object Initiated');
    };
    return {
      init: init
    };
  }();
  Site.init();

});

$(document).ready(function(){

});